
# Set form compiler representation to uflacs
import dolfin
dolfin.parameters['form_compiler']['representation'] = 'uflacs'

import pulse.geometry as geometry
import pulse.material as material
import pulse.itertarget as itertarget
import pulse.lvproblem as lvproblem
import pulse.bivproblem as bivproblem
import pulse.lvsolver as lvsolver
import pulse.postprocess as postprocess
import pulse.datacollector as datacollector

from geometry import *
from material import *
from itertarget import *
from lvproblem import *
from bivproblem import *
from lvsolver import *
from postprocess import *
from datacollector import *

__all__ = geometry.__all__ + material.__all__ + \
          ["itertarget", "itertarget2"] + lvproblem.__all__ + \
          bivproblem.__all__ + lvsolver.__all__ + postprocess.__all__ + \
          datacollector.__all__
