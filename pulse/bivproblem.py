"""This module implements a problem class for the solving of a
nonlinear mechanics problem with a biventricular mesh
"""
# Copyright (C) 2014-2015 Henrik Finsberg
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from .realspaces import *
import os.path
import numpy as np

__all__ = ["BiVProblem"]

class BiVProblem(NonlinearProblem):

    def __init__(self, geometry, material, **kwargs) :
        super(BiVProblem, self).__init__()

        p = self.default_parameters()
        p.update(**kwargs)
        self._parameters = p

        self.geo = geometry
        self.mat = material

	# activation
	self._activation()

        self._init_space()
        self._state = Function(self._Mspace)
        self._init_forms()

        self._assemble_jacobian = True

    @staticmethod
    def default_parameters() :
        p = { 'bc_type' : 'fix_base_ver',
              'control_volume' : False,
              'form_compiler_parameters' : {
                  'quadrature_degree' : 4 } }
        return p

    def F(self, b, x) :
        return

    def form(self, A, b, x) :
        ffc_param = self._parameters['form_compiler_parameters']
        if self._assemble_jacobian :
            assemble_system(self._dG, self._G, self._bcs,
                    A_tensor = A, b_tensor = b,
                    form_compiler_parameters = ffc_param)
        else :
            assemble(self._G, tensor = b, \
                     form_compiler_parameters = ffc_param)
            for bc in self._bcs :
                bc.apply(b)
        self._assemble_jacobian = not self._assemble_jacobian
        
    def J(self, A, x) :
        return

    def set_control_mode(self, mode):
        assert mode in [ "pressure", "volume" ]
        self._change_mode_and_reinit(control_volume = mode == "volume")

    def get_state(self) :
        return self._state

    def get_displacement(self) :
        return self.get_state().split()[0]

    def get_Vendo(self, vent="LV") :
        param = self._parameters
        if not param['control_volume'] :
            geo = self.geo
            state = self.get_state()
            u = split(state)[0]
            return geo.inner_volume(u, vent=vent)
        else :
	    if vent == "RV" :
                return float(param['V_endo_rv'])
	    else:
		return float(param['V_endo_lv'])

    def get_epiarea(self) :
        geo = self.geo
        state = self.get_state()
        u = split(state)[0]
        return geo.surface_area(geo.EPI, u)

    def get_pendo(self, vent = "LV") :
        param = self._parameters
        if vent == "RV":
            if param['control_volume']:
                pnum = self._Mspace.num_sub_spaces() - 2
                return get_realspace_var(self.get_state(), pnum)
            else :
                return float(param['p_endo_rv'])
        else: 
            if param['control_volume']:
                pnum = self._Mspace.num_sub_spaces() - 1
                return get_realspace_var(self.get_state(), pnum)
            else :
                 return float(param['p_endo_lv'])

    def set_Vendo(self, V, vent="LV") :
        param = self._parameters
        if not param['control_volume'] :
            raise RuntimeError('Cannot assign Vendo!')
        else :
            if vent == "RV":
                param['V_endo_rv'].assign(V)
            else:
                param['V_endo_lv'].assign(V)

    def set_pendo(self, p, vent = "LV") :
        param = self._parameters
        if vent == "RV":
            if param['control_volume']:
                pnum = self._Mspace.num_sub_spaces() - 2
                set_realspace_var(self.get_state(), pnum, p)
            else :
                param['p_endo_rv'].assign(p)
        else: 
            if param['control_volume']:
                pnum = self._Mspace.num_sub_spaces() - 1
                set_realspace_var(self.get_state(), pnum, p)
            else :
                param['p_endo_lv'].assign(p)

    def set_control_parameters(self, **kwargs) :
        p = self._parameters
        for param, value in kwargs.items() :
            if param in [ "pressure", "volume" ]:
		try:
                    vent = kwargs["vent"]
                except:
                    error("Need to specify left or right ventricle for control paramter {}".format(param))
                
		if param == "pressure" :
                    if p['control_volume'] :
                        error("Problem is in volume control mode. "\
                              "Cannot use pressure as control.")
                    self.set_pendo(value, vent)
                else :
                    if not p['control_volume'] :
                        error("Problem is in pressure control mode. "\
                              "Cannot use volume as control.")
                    self.set_Vendo(value,vent)
            else :
                if param in self.mat.parameters :
                    self.mat.parameters[param].assign(value)
                #elif param in geo.parameters :
                #    raise NotImplementedError
                else :
		    if param not in ["vent"]:
                        error("{} is not a parameter.".format(param))

    def get_control_parameter_list(self) :
        # FIXME should be independent from the specific mat and geo
        p = [ 'pressure', 'volume', 'gamma' ]
        p.extend([ k for k in self.mat.parameters.keys() if k[1] == '_' ])
        p.extend([ 'alpha_endo', 'alpha_epi', 'beta_endo', 'beta_epi' ])
        return p
        
    def get_control_parameter(self, param, vent ="LV"):
        if param == "pressure" :
            return self.get_pendo(vent = vent)
        elif param == "volume" :
            return self.get_Vendo(vent = vent)
        elif param in self.mat.parameters :
            return float(self.mat.parameters[param])
        else :
            error("{} is not a parameter.".format(param))


    def calculate_strain(self):

        print "Calculating strain"
	sfun = self.geo.sfun

	state = self.get_state()
	u = split(state)[0]
	X = SpatialCoordinate(self.geo.domain)
	x = X + u
	F = grad(x)
	
	C = F.T * F
	E = float(0.5) * (C - Identity(3))

        V = FunctionSpace(self.geo.domain, "DG", 0)
        id_func = interpolate(Expression("1"), V)

        radial, longitud, circum = self.get_cell_vectors(self.geo.domain)
        radial_strain = np.zeros(17)
        longitud_strain = np.zeros(17)
        circum_strain = np.zeros(17)

        for region in range(1,18):
            
            vol_region = assemble(id_func*dx(region, subdomain_data=sfun))
            if vol_region == 0:
                print("Region {} has zero volume".format(region))
                print("Neglecting this region, by setting strain = 0")
                r_strain = 0
                l_strain = 0
                c_strain = 0
            else:
                r_strain_form =  inner(E*radial,radial) * dx(region, subdomain_data=sfun)
                l_strain_form =  inner(E*longitud,longitud) * dx(region, subdomain_data=sfun)
                c_strain_form =  inner(E*circum, circum) * dx(region, subdomain_data=sfun)
                r_strain = assemble(r_strain_form)/vol_region
                l_strain = assemble(l_strain_form)/vol_region
                c_strain = assemble(c_strain_form)/vol_region
                print "Region: ", region, "radial: ", r_strain, "long: ", l_strain, "circum", c_strain
                radial_strain[region-1] = r_strain
                longitud_strain[region-1] = l_strain
                circum_strain[region-1] = c_strain
	
	return radial_strain, longitud_strain, circum_strain


    def get_cell_vectors(self, domain):
        """Returns local vectors corresponding to the radial, longitudinal and 
        circumferential direction. Theses are used in strain calculations
        """
	return self.geo.radial, self.geo.longitudinal, self.geo.circumferential


    def _init_space(self) :
        """Initialize spaces for the different variables

        The solution vector belogs to a mixed functions space
        
        .. math::
            
           V = V_u \times V_p \times V_{bc} \times V_{p_{innerLV}} \times V_{p_{innerRV}}
        """
        geo = self.geo
        dom = geo.domain

        vlist = []
        # Function space for displacement vector
        vlist += [ VectorFunctionSpace(dom, "P", 2, 3) ]
        # Function space for pressure
        vlist += [ FunctionSpace(dom, "P", 1) ]

        if not geo.is_axisymmetric() :
            vlist += {
                'fix_base'     : [],
                # Function space for 
                'fix_base_ver' : [ VectorFunctionSpace(dom, "Real", 0, 3) ],
                'free'         : [ VectorFunctionSpace(dom, "Real", 0, 6) ],
                }[self._parameters['bc_type']]
        else :
            vlist += {
                'fix_base'     : [],
                'fix_base_ver' : [ FunctionSpace(dom, "Real", 0) ],
                'fix_epi_area' : [ VectorFunctionSpace(dom, "Real", 0, 2) ],
                'free'         : [ VectorFunctionSpace(dom, "Real", 0, 2) ],
                }[self._parameters['bc_type']]

        if self._parameters['control_volume'] :
            # Function space for the endocardial pressure in the rv
            vlist += [ FunctionSpace(dom, "Real", 0) ]
            # Function space for the endocardial pressure in the lv
            vlist += [ FunctionSpace(dom, "Real", 0) ]

        self._Mspace = MixedFunctionSpace(vlist)

    def _init_forms(self) :
        "Initialize different forms, such as "
        param = self._parameters
        geo = self.geo
        mat = self.mat

        # unknowns
        state = self.get_state()
        u = split(state)[0]
        p = split(state)[1]

        if param['control_volume'] :
            pendo_rv = split(state)[-2]
            param['V_endo_rv'] = Constant(geo.inner_volume(vent='RV'))
            Vendo_rv = param['V_endo_rv']

            pendo_lv = split(state)[-1]
            param['V_endo_lv'] = Constant(geo.inner_volume(vent='LV'))
            Vendo_lv = param['V_endo_lv']
        else :
            param['p_endo_rv'] = Constant(0.0)
            pendo_rv = param['p_endo_rv']
            Vendo_rv = None

            param['p_endo_lv'] = Constant(0.0)
            pendo_lv = param['p_endo_lv']
            Vendo_lv = None

        # deformation gradient tensor
        # ---------------------------
        if geo.is_axisymmetric() :
            # axisymmetric formulation
            X = SpatialCoordinate(geo.domain)
            Z, R, T = X[0], X[1], 0.0
            Jgeo = 2.0 * DOLFIN_PI * R

            z, r, t = Z + u[0], R + u[1], T + u[2]

            # deformation gradient tensor
            F = as_tensor([[ z.dx(0), z.dx(1), 0.0 ],
                           [ r.dx(0), r.dx(1), 0.0 ],
                           [ t.dx(0), t.dx(1), 1.0 ]])

            # normalization of components
            F = diag(as_vector([ 1, 1, r ])) * F
            F = F * diag(as_vector([ 1, 1, 1/R ]))
        else :
            Jgeo = 1.0
            F = Identity(3) + grad(u)

        # internal energy
        # ---------------
        L = mat.strain_energy(F, p) * Jgeo * dx

        # inner pressure
        # --------------
        L += self._inner_volume_constraint(u, pendo_lv, Vendo_lv, geo.ENDO_LV)
	L += self._inner_volume_constraint(u, pendo_rv, Vendo_rv, geo.ENDO_RV)

        # bcs
        # ---
        if geo.is_axisymmetric() :
            if param['bc_type'] == 'fix_base' :
                # fixed base plus symmetry at apex
                Vsp = self._Mspace.sub(0)
                bcs = [ DirichletBC(Vsp, Constant((0.0, 0.0, 0.0)), geo.bfun, geo.BASE),
                        DirichletBC(Vsp.sub(1), Constant(0.0), geo.bfun, geo.APEX) ]
            elif param['bc_type'] == 'fix_base_ver' :
                Vux = self._Mspace.sub(0).sub(0)
                Vuy = self._Mspace.sub(0).sub(1)
                bcs = [ DirichletBC(Vux, Constant(0.0), geo.bfun, geo.BASE),
                        DirichletBC(Vuy, Constant(0.0), geo.bfun, geo.APEX) ]
                c = split(state)[2]
                L += inner(u[2], c) * Jgeo * dx
            elif param['bc_type'] == 'fix_epi_area' :
                Vuy = self._Mspace.sub(0).sub(1)
                bcs = [ DirichletBC(Vuy, Constant(0.0), geo.bfun, geo.APEX) ]
                c = split(state)[2]
                L += inner(u[2], c[0]) * Jgeo * dx
                #L += inner(c[1], c[1]) * Jgeo * dx
                # area constraint
                pepi = c[1]
                Aepi = self.geo.surface_area(self.geo.EPI)
                L += self._surface_area_constraint(u, pepi, Aepi, geo.EPI)
            elif param['bc_type'] == 'free' :
                # symmetry condition
                Vuy = self._Mspace.sub(0).sub(1)
                bcs = [ DirichletBC(Vuy, Constant(0.0), geo.bfun, geo.APEX) ]
                # no axial rotations and translation
                c = split(state)[2]
                L += inner(u[2], c[0]) * Jgeo * dx
                L += inner(u[0], c[1]) * Jgeo * dx
            else :
                raise NotImplementedError
        else :
            if param['bc_type'] == 'fix_base_ver' :
                c = split(state)[2]
                # no traslations in xy-plane
                ct = as_vector([ c[0], c[1], 0.0 ])
                # no rotations around z-axis
                cr = as_vector([ 0.0, 0.0, c[2] ])
                L += self._rigid_motion_constraint(u, ct, cr)
                # no vertical displacement at the base
                Vsp = self._Mspace.sub(0).sub(2)
                bcs =[  DirichletBC(Vsp, Constant(0.0), geo.bfun, geo.BASE) ]
            elif param['bc_type'] == 'fix_base' :
                # no displacement at the base
                Vsp = self._Mspace.sub(0)
                bcs = [ DirichletBC(Vsp, Constant((0.0, 0.0, 0.0)), geo.bfun, geo.BASE) ]
            else :
                raise NotImplementedError

        # tangent problem
        # ---------------
        # test and trial *required* because of a bug in DOLFIN
        # when domain is coordinate-specific (domain obtained from
        # the function is different from the real domain).
        self._G   = derivative(L, self.get_state(), TestFunction(self._Mspace))
        self._dG  = derivative(self._G, self.get_state(), TrialFunction(self._Mspace))
        self._bcs = bcs

    
    def _activation(self) :
        """
	Compute activation parameter.
        Initially we assume that gamma(x,y,z,t) can be written as f(t)*g(x,y,z)
        This function thus plays the role as g
        """
	domain = self.geo.domain
	V = FunctionSpace(domain, "CG", 1)
	f = Expression('A*exp(-(pow(x[0]-x1, 2)+pow(x[1]-x2, 2))/(2*sigma) )'\
                       , A=1, sigma=30, x1 = 8, x2 = -3)

	self.gamma = interpolate(f,V)



    def _inner_volume_constraint(self, u, p, V, sigma) :
        """
        Compute the form
            (V(u) - V, p) * ds(sigma)
        where V(u) is the volume computed from u and
            u = displacement
            V = volume enclosed by sigma
            p = Lagrange multiplier
        sigma is the boundary of the volume.
        """

        geo = self.geo
        dom = geo.domain
        dim = dom.geometric_dimension()

        # ufl doesn't support any measure for duality
        # between two Real spaces, so we have to divide
        # by the total measure of the domain
        ds_sigma = ds(sigma, domain = dom, subdomain_data = geo.bfun)
        area = assemble(Constant(1.0) * ds_sigma)

        V_u = geo.inner_volume_form(u)
        L = - p * V_u * ds_sigma

        if V is not None :
            L += Constant(1.0/area) * p * V * ds_sigma

        return L

    def _surface_area_constraint(self, u, p, A, sigma) :
        geo = self.geo
        dom = geo.domain
        dim = dom.geometric_dimension()

        ds_sigma = ds(sigma, domain = dom, subdomain_data = geo.bfun)
        refarea = assemble(Constant(1.0) * ds_sigma)

        A_u = geo.surface_area_form(u)
        L = - p * A_u * ds_sigma

        if A is not None :
            L += Constant(1.0/refarea) * p * A * ds_sigma

        return L

    def _rigid_motion_constraint(self, u, ct, cr) :
        """
        Compute the form
            (u, ct) * dx + (cross(u, X), cr) * dx
        where
            u  = displacement
            ct = Lagrange multiplier for translations
            ct = Lagrange multiplier for rotations
        """

        dom = self.geo.domain
        dim = dom.geometric_dimension()
        X = SpatialCoordinate(dom)

        Lt = inner(ct, u) * dx

        if dim == 2 :
            # rotations around z
            Lr = inner(cr, X[0]*u[1] - X[1]*u[0]) * dx
        elif dim == 3:
            # rotations around x, y, z
            Lr = inner(cr, cross(X, u)) * dx

        return Lt + Lr

    def _change_mode_and_reinit(self, control_volume = False):
        if self._parameters['control_volume'] == control_volume :
            return
        # save the current state
        state_old = self.get_state().copy(True)
        pendo_old_lv = self.get_pendo("LV")
        Vendo_old_lv = self.get_Vendo("LV")
	pendo_old_rv = self.get_pendo("RV")
        Vendo_old_rv = self.get_Vendo("RV")
        # reinit problem
        self._parameters['control_volume'] = control_volume
        self._init_space()
        self._state = Function(self._Mspace)
        self._init_forms()

        # assign old values
        assign(self.get_state().sub(0), state_old.sub(0))
        assign(self.get_state().sub(1), state_old.sub(1))
        if self._parameters['bc_type'] not in [ 'fix_endoring', 'fix_base' ] \
           and not self.geo.is_axisymmetric() :
            assign(self.get_state().sub(2), state_old.sub(2))
        self.set_pendo(pendo_old_lv, "LV")
	self.set_pendo(pendo_old_rv, "RV")
        if control_volume :
            self.set_Vendo(Vendo_old_lv, "LV")
	    self.set_Vendo(Vendo_old_rv, "RV")

    def postprocess(self, numsol) :

        labels = [ "I_1", "I_4f", "I_4s", "I_8fs",
                   "W_1", "W_4f", "W_4s", "W_8fs",
                   "T_1", "T_4f", "T_4s", "T_8fs",
                   "T_ll", "T_mm", "T_tt", "T_lm", "T_lt", "T_mt",
                   "E_1", "E_4f", "E_4s", "E_8fs",
                   "E_ll", "E_mm", "E_tt", "E_lm", "E_lt", "E_mt",
                   "tau_v1", "tau_v2" ]

        if numsol < 0 :
            return labels

        # opens the solution
        state = self.mech_state
        # FIXME temporary!
        #if numsol == 0 :
        #    self.time_series.read(state, "solution_%s" % str(numsol).zfill(3))
        #else :
        #    self.time_series.read(state, "solution_%s" % str(numsol+1).zfill(3))

        self.time_series.read(state, "solution_%s" % str(numsol).zfill(3))
        self.time = self.history[numsol][0]
        self.update_state()

        # P2 -> P1 on refined mesh
        mesh = refine(self.geo.mesh)
        CG1v = VectorFunctionSpace(mesh, "CG", 1)
        CG1s = FunctionSpace(mesh, "CG", 1)

        u = interpolate(state.split()[0], CG1v)
        p = interpolate(state.split()[1], CG1s)

        # grad(u) is DG0 tensor function.
        # FIXME should be recovered into CG1 tensor
        # without projection
        #DG0t = TensorFunctionSpace(mesh, "DG", 0)
        #grad_u = local_project(grad(u), DG0t)

        CG1t = TensorFunctionSpace(mesh, "CG", 1)
        grad_u = project(grad(u), CG1t)

        # microstructure
        f0 = self.mat.fibers
        s0 = self.mat.sheets

        mat_params_str = "\n".join(["const double %s = %f;" % (uu,vv) for uu,vv in self.mat.params.iteritems()])

        # evaluator code
        if not hasattr(self, "active_only_fibers") :
            energy_code = """
            double W_1   = a_iso/(2.0*b_iso) * (std::exp(b_iso*(Ie_1 - 3)) - 1);
            double W_4f  = a_fibers/(2.0*b_fibers) * (std::exp(b_fibers*pow(subplus(Ie_4f - 1), 2)) - 1);
            double W_4s  = a_sheets/(2.0*b_sheets) * (std::exp(b_sheets*pow(subplus(Ie_4s - 1), 2)) - 1);
            double W_8fs = a_cross/(2.0*b_cross) * (std::exp(b_cross*pow(Ie_8fs, 2)) - 1);

            double dW_1   = a_iso/2.0 * std::exp(b_iso*(Ie_1 - 3));
            double dW_4f  = a_fibers * subplus(Ie_4f - 1.0)
                                 * std::exp(b_fibers*pow(Ie_4f - 1, 2));
            double dW_4s  = a_sheets * subplus(Ie_4s - 1.0)
                                 * std::exp(b_sheets*pow(Ie_4s - 1, 2));
            double dW_8fs = a_cross * Ie_8fs * std::exp(b_cross*pow(Ie_8fs, 2));

            matrix_type T = 2.0 * dW_1  * Be
                                + 2.0 * dW_4f * (fe * fe.transpose())
                                + 2.0 * dW_4s * (se * se.transpose())
                                + dW_8fs * (se * fe.transpose() + fe * se.transpose())
                                + pressure * I;
            """
        else :
            energy_code = """
            double W_1   = a_iso/(2.0*b_iso)   * (std::exp(b_iso*(I_1 - 3)) - 1);
            double W_4f  = a_fibers/(2.0*b_fibers) * (std::exp(b_fibers*pow(subplus(Ie_4f - 1), 2)) - 1);
            double W_4s  = a_sheets/(2.0*b_sheets) * (std::exp(b_sheets*pow(subplus(I_4s - 1), 2)) - 1);
            double W_8fs = a_cross/(2.0*b_cross) * (std::exp(b_cross*pow(I_8fs, 2)) - 1);

            double dW_1   = a_iso/2.0 * std::exp(b_iso*(I_1 - 3));
            double dW_4f  = a_fibers * subplus(Ie_4f - 1.0)
                                 * std::exp(b_fibers*pow(Ie_4f - 1, 2));
            double dW_4s  = a_sheets * subplus(I_4s - 1.0)
                                 * std::exp(b_sheets*pow(I_4s - 1, 2));
            double dW_8fs = a_cross * I_8fs * std::exp(b_cross*pow(I_8fs, 2));

            matrix_type T = 2.0 * dW_1  * B
                                + 2.0 * dW_4f * (fe * fe.transpose())
                                + 2.0 * dW_4s * (s * s.transpose())
                                + dW_8fs * (s * f.transpose() + f * s.transpose())
                                + pressure * I;
            """

        common_code = """
        #include <Eigen/Dense>

        class Evaluator : public Expression
        {
        public :

        // types
        typedef Eigen::Vector3d vector_type;
        typedef Eigen::Matrix3d matrix_type;

        // members
        boost::shared_ptr<dolfin::GenericFunction> grad_u;
        boost::shared_ptr<dolfin::Expression> f0;
        boost::shared_ptr<dolfin::Expression> s0;
        boost::shared_ptr<dolfin::Expression> e_lambda;
        boost::shared_ptr<dolfin::Expression> e_mu;
        boost::shared_ptr<dolfin::Expression> e_theta;
        boost::shared_ptr<dolfin::GenericFunction> p;

        double gamma;

        Evaluator () : Expression(30)
        {}

        inline double subplus(const double x) const
        {
            return (x > 0) ? x : 0.0;
        }

        void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x,
                  const ufc::cell& cell) const
        {
            // Check if grad_u has been assigned
            assert(grad_u);
            assert(f0);
            assert(s0);
            assert(e_lambda);
            assert(e_mu);
            assert(e_theta);
            assert(p);

            // Check if grad_u is a tensor-valued function
            assert(grad_u->value_rank() == 2);
            assert(f0->value_rank() == 1);
            assert(s0->value_rank() == 1);
            assert(e_lambda->value_rank() == 1);
            assert(e_mu->value_rank() == 1);
            assert(e_theta->value_rank() == 1);
            assert(p->value_rank() == 0);

            // Flat value array
            dolfin::Array<double> grad_u_val(grad_u->value_size());
            dolfin::Array<double> f0_val(f0->value_size());
            dolfin::Array<double> s0_val(s0->value_size());
            dolfin::Array<double> e_lambda_val(e_lambda->value_size());
            dolfin::Array<double> e_mu_val(e_mu->value_size());
            dolfin::Array<double> e_theta_val(e_theta->value_size());
            dolfin::Array<double> p_val(p->value_size());

            // Evaluation
            grad_u->eval(grad_u_val, x, cell);
            f0->eval(f0_val, x, cell);
            s0->eval(s0_val, x, cell);
            e_lambda->eval(e_lambda_val, x, cell);
            e_mu->eval(e_mu_val, x, cell);
            e_theta->eval(e_theta_val, x, cell);
            p->eval(p_val, x, cell);

            double pressure = p_val[0];

            // Deformation gradient tensor
            matrix_type GRAD_u = Eigen::Map<matrix_type>(grad_u_val.data());

            matrix_type I = matrix_type::Identity();
            matrix_type F = I + GRAD_u;
            matrix_type C = F.transpose() * F;
            matrix_type B = F * F.transpose();

            vector_type f0_ = Eigen::Map<vector_type>(f0_val.data());
            vector_type s0_ = Eigen::Map<vector_type>(s0_val.data());

            vector_type e_lambda_ = Eigen::Map<vector_type>(e_lambda_val.data());
            vector_type e_mu_     = Eigen::Map<vector_type>(e_mu_val.data());
            vector_type e_theta_  = Eigen::Map<vector_type>(e_theta_val.data());

            matrix_type f0f0 = f0_ * f0_.transpose();
            matrix_type Fa = (1.0-gamma)*f0f0 + 1./std::sqrt(1.0-gamma)*(I - f0f0);
            matrix_type Fe = F * Fa.inverse();
            matrix_type Ce = Fe.transpose() * Fe;
            matrix_type Be = Fe * Fe.transpose();

            // Fibers and sheets
            vector_type f = F * f0_;
            vector_type s = F * s0_;
            vector_type fe = Fe * f0_;
            vector_type se = Fe * s0_;

            // Invariants
            double I_1 = C.trace();
            double I_4f = f.dot(f);
            double I_4s = s.dot(s);
            double I_8fs = f.dot(s);

            double Ie_1 = Ce.trace();
            double Ie_4f = fe.dot(fe);
            double Ie_4s = se.dot(se);
            double Ie_8fs = fe.dot(se);

            // Cauchy tensor
            %(mat_params)s
            /*
            const double a_1   = 0.2362e-1, b_1   = 10.810;
            const double a_4f  = 20.037e-1, b_4f  = 14.154;
            const double a_4s  = 3.7245e-1, b_4s  = 5.1645;
            const double a_8fs = 0.4108e-1, b_8fs = 11.300;
            */

            // Energy and stress
            %(energy_code)s

            matrix_type E = 0.5 * (C - I);
            double Ell = (E*e_lambda_).dot(e_lambda_);
            double Emm = (E*e_mu_).dot(e_mu_);
            double Ett = (E*e_theta_).dot(e_theta_);
            double Elm = (E*e_lambda_).dot(e_mu_);
            double Elt = (E*e_lambda_).dot(e_theta_);
            double Emt = (E*e_mu_).dot(e_theta_);

            double Tll = (T*e_lambda_).dot(e_lambda_);
            double Tmm = (T*e_mu_).dot(e_mu_);
            double Ttt = (T*e_theta_).dot(e_theta_);
            double Tlm = (T*e_lambda_).dot(e_mu_);
            double Tlt = (T*e_lambda_).dot(e_theta_);
            double Tmt = (T*e_mu_).dot(e_theta_);

            // Invariants
            values[0] = I_1;
            values[1] = I_4f;
            values[2] = I_4s;
            values[3] = I_8fs;

            // Energy
            values[4] = W_1;
            values[5] = W_4f;
            values[6] = W_4s;
            values[7] = W_8fs;

            // Stress
            values[8]  = T.trace();
            values[9]  = (T*f.normalized()).dot(f.normalized());
            values[10] = (T*s.normalized()).dot(s.normalized());
            values[11] = (T*s.normalized()).dot(f.normalized());
            values[12] = Tll;
            values[13] = Tmm;
            values[14] = Ttt;
            values[15] = Tlm;
            values[16] = Tlt;
            values[17] = Tmt;

            // Strain
            values[18] = E.trace();
            values[19] = (E*f.normalized()).dot(f.normalized());
            values[20] = (E*s.normalized()).dot(s.normalized());
            values[21] = (E*s.normalized()).dot(f.normalized());
            values[22] = Ell;
            values[23] = Emm;
            values[24] = Ett;
            values[25] = Elm;
            values[26] = Elt;
            values[27] = Emt;

            // Torsion (v1)
            values[28] = std::asin(2.0*Emt/std::sqrt((1+2.0*Emm)*(1+2.0*Ett))) * 180.0 / DOLFIN_PI;
            // Torsion (v2)
            values[29] = 0.0;
        }

        };
        """ % { "energy_code" : energy_code, "mat_params" : mat_params_str }

        from textwrap import dedent

        CG1v = VectorFunctionSpace(mesh, "CG", 1, 30)
        evaluator_expr = Expression(cppcode = dedent(common_code), \
                                    element = CG1v.ufl_element())
        evaluator_expr.gamma = float(self.gamma)
        evaluator_expr.grad_u = grad_u
        evaluator_expr.f0 = f0
        evaluator_expr.s0 = s0
        evaluator_expr.e_lambda = Expression(cppcode = self.geo.e_lambda)
        evaluator_expr.e_mu = Expression(cppcode = self.geo.e_mu)
        evaluator_expr.e_theta = Expression(cppcode = self.geo.e_theta)
        evaluator_expr.p = p

        evaluator = interpolate(evaluator_expr, CG1v)

        return mesh, u, p, evaluator

