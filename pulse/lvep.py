from fenics import *
from fenicshotools.vtkutils import *
from pulse.geometry import LVProlateEllipsoid
from textwrap import dedent
import os.path

class LVEPProblem(NonlinearProblem) :
    def __init__(self, geo, param='') :
        param = param or {}
        self._parameters = self.default_parameters()
        self._parameters.update(param)

        if geo.is_axisymmetric() :
            _, R = SpatialCoordinate(geo.domain)
            Jgeo = 2*DOLFIN_PI*R
            Grad = lambda u : as_vector([ grad(u)[0], grad(u)[1], 0.0 ])
        else :
            Jgeo = 1.0
            Grad = lambda u : grad(u)

        self._geo = geo
        self._Grad = Grad
        self._Jgeo = Jgeo

        # function space for potential
        V = FunctionSpace(geo.domain, 'P', 1)

        # initial condition (projected on V)
        u0 = self.initial_condition(V)

        # the problem
        muf = Constant(self._parameters['conductivity_fibers'],
                       name='conductivity_fibers')
        mut = Constant(self._parameters['conductivity_transverse'],
                       name='conductivity_transverse')

        I = Identity(3)
        f0 = geo.f0
        M = muf*outer(f0, f0) + mut*(I - outer(f0, f0))

        u = Function(V, name="potential")
        uold = Function(V)

        a = Constant(1000.0)
        alpha = Constant(0.2)
        deltat = Constant(0.002)

        v = TestFunction(V)

        F = 1/deltat*(u - uold)*v*Jgeo*dx \
          + inner(Grad(u), M*Grad(v))*Jgeo*dx \
          + a*uold*(uold-1)*(uold-alpha)*v*Jgeo*dx

        dF = derivative(F, u, TrialFunction(V))

        # setting variables
        self._F  = F
        self._dF = dF
        self.u = u
        self.uold = uold
        self.u0 = u0
        self.bcs = []

        super(LVEPProblem, self).__init__()

        self._assembleJ = True

    @staticmethod
    def default_parameters() :
        return { 'Cm' : 1.0,
                 'conductivity_fibers' : 1.0,
                 'conductivity_transverse' : 1.0,
                 'kappa' : 8.0,
                 'alpha' : 0.1,
                 'epsilon' : 0.01,
                 'mu_1' : 0.12,
                 'mu_2' : 0.3,
                 'beta' : 0.1,
                 'kappa_Ta' : 0.0,
                 'e0' : 1.0 }

    def F(self, b, x) :
        assemble(self._F, tensor=b)

    def J(self, A, x) :
        if self._assembleJ :
            assemble(self._dF, tensor=A)
            self._assembleJ = False

    def _compile_initial_condition_code(self) :
        geo = self._geo
        pos = geo._normalized_transmural_position_code()
        code = dedent(\
        """
        class InitialCondition : public Expression
        {{
        public :

            typedef Eigen::Vector3d vec_type;
            typedef Eigen::Matrix3d mat_type;

            std::shared_ptr<dolfin::Expression> cart2coords;

            InitialCondition() : Expression()
            {{}}

            void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& raw_x,
                      const ufc::cell& cell) const
            {{
                // check if coordinates are ok
                assert(this->cart2coords);

                // first find (lambda, mu, theta) from (x0, x1, x2)
                dolfin::Array<double> coords(3);
                this->cart2coords->eval(coords, raw_x, cell);
                double mu = coords[1];

                // transmural position
                double pos = 0.0;
                {pos}

                values[0] = (mu <= DOLFIN_PI/3) * (pos <= 0.1);
            }}
        }};
        """).format(pos=pos)

        return code

    def initial_condition(self, V) :
        # coordinate mapper
        geo = self._geo
        coords = Expression(cppcode=geo._compile_cart2coords_code())
        if geo.domain.coordinates() :
            coords.coords = geo.domain.coordinates()

        # initial condition expression
        code = self._compile_initial_condition_code()
        uexpr = Expression(cppcode=code)
        uexpr.cart2coords = coords

        return Function(interpolate(uexpr, V), name='initial condition')

if __name__ == "__main__" :

    set_log_level(DEBUG)

    # geometry
    comm = mpi_comm_world()
    ndiv, axisym = 16, True
    gfile = 'lvep_n{}_{}_geo.h5'.format(ndiv, 'axisym' if axisym else 'full')
    geoparam = LVProlateEllipsoid.default_parameters()
    geoparam['axisymmetric'] = axisym
    geoparam['mesh_generation']['order'] = 1
    geoparam['mesh_generation']['ndiv'] = ndiv
    geoparam['microstructure']['function_space'] = 'Quadrature_1'
    regen = not os.path.isfile(gfile)
    geo = LVProlateEllipsoid(gfile, '', comm, regen, parameters=geoparam)

    pb = LVEPProblem(geo)
    exit(0)

    solver = PETScSNESSolver()
    solver.parameters["linear_solver"] = "gmres"
    solver.parameters["report"] = False
    solver.parameters["lu_solver"]["same_nonzero_pattern"] = True
    solver.parameters["lu_solver"]["symmetric"] = True
    solver.parameters["lu_solver"]["reuse_factorization"] = True
    PETScOptions.set("snes_monitor")
    PETScOptions.set("ksp_type", "preonly")
    PETScOptions.set("pc_type", "cholesky")
    PETScOptions.set("pc_factor_mat_solver_package", "cholmod")
    PETScOptions.set("mat_mumps_icntl_7", 6)

    ofile = File("testpot.xdmf")
    pb.u.vector()[:] = pb.u0.vector()
    ofile << (pb.u, 0.0)
    for i in xrange(1, 500) :
        pb.uold.vector()[:] = pb.u.vector()
        solver.solve(pb, pb.u.vector())
        ofile << (pb.u, float(i))

    #vtkgrid = dolfin2vtk(geo.domain)
    #vtk_add_field(vtkgrid, pb.u)
    #dim = 2 if geo.is_axisymmetric() else 3
    #vtk_write_file(vtkgrid, 'test_lvep_{}d.vtu'.format(dim))

