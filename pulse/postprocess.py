"""This module implements functions to post process data 
"""
# Copyright (C) 2014 Simone Pezzuto
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.
#

import sys
from dolfin import *
from material import HolzapfelOgden
from pulse import *
import math as m
import numpy as np

__all__ = ["save_torsion_section", "save_torsion_longitudinal",
           "ventricle_postprocess", "plot_pvloop"]

def line_prepender(filename,line):
    with open(filename,'r+') as f:
        content = f.read()
        f.seek(0,0)
        f.write(line.rstrip('\r\n') + '\n' + content)


class ProlateCoord(object):
    
    def __init__(self, param_set):
    
        # geometrical parameters
        self.dfocal  = param_set["d_focal"]
        self.mu_base = param_set["mu_base"]
        self.l_endo  = param_set["l_endo"]
        self.l_epi   = param_set["l_epi"]

        b_endo = self.dfocal * m.cosh(self.l_endo)
        b_epi  = self.dfocal * m.cosh(self.l_epi)

        # this is necessary since the ventricle is translated
        # to have the base at z = 0
        mu_endo_base = self.mu_base
        mu_epi_base  = m.acos(b_endo/b_epi * m.cos(mu_endo_base))
        self.z_endo_base  = b_endo * m.cos(mu_endo_base)

    def tocart(self, lmbda, mu, theta):

        x = self.dfocal * m.sinh(lmbda) * sin(mu) * cos(theta)
        y = self.dfocal * m.sinh(lmbda) * sin(mu) * sin(theta)
        z = self.dfocal * m.cosh(lmbda) * cos(mu) - self.z_endo_base

        return np.array([x, y, z])

    
    def r_endo(self, z):
        mu = m.acos((z + self.z_endo_base) / (self.dfocal * m.cosh(self.l_endo)))
        return self.dfocal * m.sinh(self.l_endo) * m.sin(mu)

    def r_epi(self, z):
        mu = m.acos((z + self.z_endo_base) / (self.dfocal * m.cosh(0.98*self.l_epi)))
        return self.dfocal * m.sinh(0.98*self.l_epi) * m.sin(mu)


def select_timestep(ventricle, step):

    state = ventricle.mech_state
    ventricle.time_series.read(state, "solution_%s" % str(step).zfill(3))
    ventricle.update_state()


def save_torsion_section(ventricle, filename, time, z):

    # coordinates converter
    param_set = ventricle.geo.parameters
    pcoord = ProlateCoord(param_set)

    # the solution
    select_timestep(ventricle, time)
    u = ventricle.mech_state.split()[0]

    # section
    r_endo = pcoord.r_endo(z)
    r_epi  = pcoord.r_epi(z)

    with open(filename, 'w') as ofile:

        for r in np.linspace(r_endo, r_epi, 101):
            X = np.array([r, 0.0, z])
            x = X + u(X)

            phi = m.atan2(x[1], x[0]) * 180.0 / DOLFIN_PI

            # FIXME: Use write!
            print>>ofile, "%f %f" % (r, phi)


def save_torsion_longitudinal(ventricle, filename, time, where = "endo"):

    # coordinates converter
    param_set = ventricle.geo.parameters
    pcoord = ProlateCoord(param_set)

    # the solution
    select_timestep(ventricle, time)
    u = ventricle.mech_state.split()[0]

    if where == "endo":
        lmbda = pcoord.l_endo
    elif where == "epi":
        lmbda = 0.98*pcoord.l_epi

    mu_base = 0.95 * pcoord.mu_base
    mu_apex = m.pi/12.

    with open(filename, 'w') as ofile:

        for mu in np.linspace(mu_base, mu_apex, 101):
            X = pcoord.tocart(lmbda, mu, 0.0)
            x = X + u(X)

            phi = m.atan2(x[1], x[0]) * 180.0 / DOLFIN_PI
            R = m.sqrt(X[0]*X[0] + X[1]*X[1])
            
            # FIXME: Use write!
            print>>ofile, "%f %f %f" % (X[2], R, phi)


def ventricle_postprocess(ventricle, output_dir):

    for time, label in [ (0, "edp"), (11, "avo"), (21, "esp"), (26, "mvo") ]:
        basename = '/Users/simone/torsion_%s' % label
        save_torsion_section(ventricle, basename + '_base.txt', time, 0.8)
        save_torsion_section(ventricle, basename + '_apex.txt', time, 5.5)
        save_torsion_longitudinal(ventricle, basename + '_endo.txt', time, "endo")
        save_torsion_longitudinal(ventricle, basename + '_epi.txt', time, "epi")

    sys.exit()

    out_basedir = '/Users/simone/'
    basename = 'prolate_paper'

    # time | p_endo | v_endo | gamma
    pvloop = ventricle.history

    # ================
    # extended pV-loop
    # ----------------
    numstep = len(pvloop[:,0])

    # geometrical parameters
    param_set = ventricle.geo.parameters
    dfocal  = param_set["d_focal"]
    mu_base = param_set["mu_base"]
    l_endo  = param_set["l_endo"]
    l_epi   = param_set["l_epi"]

    b_endo = dfocal * m.cosh(l_endo)
    b_epi = dfocal * m.cosh(l_epi)

    # this is necessary since the ventricle is translated
    # to have the base at z = 0
    mu_endo_base = mu_base
    mu_epi_base  = m.acos(b_endo/b_epi * m.cos(mu_endo_base))
    z_endo_base  = b_endo * m.cos(mu_endo_base)

    # exported vtk files
    labels = ventricle.postprocess(-1)
    ofiles = []
    for l in labels:
        ofiles.append(File("%s/prolatepost/%s.xdmf" % (output_dir, l)))
    
    # for each time step
    pvext = []
    for tstep in xrange(0, numstep):
        time = pvloop[tstep, 0]
        info(":: STEP %d (time %f)::" % (tstep, time))

        # computing the evaluator
        mesh, u, pressure, evaluator = ventricle.postprocess(tstep)
        for kk in xrange(0, 30):
            ofiles[kk] << (evaluator.split()[kk], time)

        # for a fixed section and angle, computing
        # each quantity transmurally
        theta = 0.0
        mu_base = m.pi/2.0
        mu_apex = m.pi/6.0

        nsamples = 101
        lmbda_span = np.linspace(l_endo, 0.995*l_epi, nsamples)
        values = np.zeros([len(lmbda_span), len(labels) + 3])

        for kk, lmbda in enumerate(lmbda_span):
            # (lambda, mu, theta) coordinates
            xp_base = dfocal * m.sinh(lmbda) * sin(mu_base) * cos(theta)
            yp_base = dfocal * m.sinh(lmbda) * sin(mu_base) * sin(theta)
            zp_base = dfocal * m.cosh(lmbda) * cos(mu_base) - z_endo_base
            pp_base = Point(xp_base, yp_base, zp_base)
            xp_apex = dfocal * m.sinh(lmbda) * sin(mu_apex) * cos(theta)
            yp_apex = dfocal * m.sinh(lmbda) * sin(mu_apex) * sin(theta)
            zp_apex = dfocal * m.cosh(lmbda) * cos(mu_apex) - z_endo_base
            pp_apex = Point(xp_apex, yp_apex, zp_apex)
            
            # evaluating the function
            v0 = np.zeros(30)
            evaluator(pp_base, values = v0)

            values[kk, 0:30] = v0

            # torsion based on deformation
            X_base = np.array([xp_base, yp_base, zp_base])
            X_apex = np.array([xp_apex, yp_apex, zp_apex])

            x_base = X_base + u(pp_base)
            x_apex = X_apex + u(pp_apex)

            # in-plane radius
            R_base = np.linalg.norm(X_base[0:2])
            r_base = np.linalg.norm(x_base[0:2])
            R_apex = np.linalg.norm(X_apex[0:2])
            r_apex = np.linalg.norm(x_apex[0:2])

            # distance between sections
            dist = abs(x_base[2] - x_apex[2])

            # in-plane angle
            phi_base = asin(np.cross(X_base, x_base)[2]/(R_base * r_base))
            phi_apex = asin(np.cross(X_apex, x_apex)[2]/(R_apex * r_apex))

            torsion = 1./dist*(phi_apex*r_apex - phi_base*r_base) / DOLFIN_PI * 180.0

            values[kk, 29] = torsion
            values[kk, 30] = r_base
            values[kk, 31] = r_apex

            # pressure
            values[kk, 32] = pressure(pp_base)


        # longitudinal deformation
        zbottom = dfocal * m.cosh(0.995*l_epi) - z_endo_base
        height = zbottom + u(Point(0.0, 0.0, zbottom))[2]

        # energy is integrated over the domain
        W_1   = assemble(evaluator.split()[4] * dx)
        W_4f  = assemble(evaluator.split()[5] * dx)
        W_4s  = assemble(evaluator.split()[6] * dx)
        W_8fs = assemble(evaluator.split()[7] * dx)
        W = W_1 + W_4f + W_4s + W_8fs

        # exporting
        pvext_row  = pvloop[tstep,:].tolist()
        pvext_row += values[:,0:30].min(0).tolist()
        pvext_row += values[:,0:30].mean(0).tolist()
        pvext_row += values[:,0:30].max(0).tolist()
        pvext_row += values[0,0:30].tolist()
        pvext_row += values[nsamples/2,0:30].tolist()
        pvext_row += values[-1,0:30].tolist()
        pvext_row += [ height ]
        pvext_row += values[[0,-1],30].tolist()
        pvext_row += values[[0,-1],31].tolist()
        pvext_row += [ W, W_1, W_4f, W_4s, W_8fs ]
        pvext_row += [ values[:, 32].min(), values[:, 32].mean(), values[:, 32].max() ]
        pvext_row += values[[0,nsamples/2,-1],32].tolist()

        print(pvext_row)
        pvext.append(pvext_row)

    # exported table
    pvext_file  = output_dir + "/prolatepost/%s_pvloop.txt" % basename
    
    pvext_labels  = [ "time", "pressure", "volume", "gamma" ]
    sublabels = ["min", "avg", "max", "endo", "mid", "epi" ]
    pvext_labels += [ "_".join([l, ll]) for ll in sublabels for l  in labels ]
    pvext_labels += [ "height", "r_base_endo", "r_base_epi", "r_apex_endo", "r_apex_epi" ]
    pvext_labels += [ "W", "W_1", "W_4f", "W_4s", "W_8fs" ]
    pvext_labels += [ "pinc_min", "pinc_avg", "pinc_max", "pinc_endo", "pinc_mid", "pinc_epi" ]

    pvext_head = " ".join(pvext_labels)

    np.savetxt(pvext_file, pvext)
    line_prepender(pvext_file, pvext_head)
 

def plot_pvloop(picklepath):
    # FIXME: Make general. Depends on the pickled savings. We need
    # more meta information so we can save and load data more
    # generally
    import cPickle as pickle
    import matplotlib.pyplot as plt
    f = open(picklepath)
    params = pickle.load(f)
    prv = []
    plv = []
    Vrv = []    
    Vlv = []
    gamma = []

    for i in range(len(params)):
	plv.append(1000*params[i][0])
	#prv.append(1000*params[i][1])
	Vlv.append(params[i][1])
	#Vrv.append(params[i][3])
	#gamma.append(params[i][4])

    plt.figure()
    plt.plot(Vlv, plv, 'ro-')
    plt.xlabel("Volume [ml]")
    plt.ylabel("Pressure kPa")
    plt.show()
