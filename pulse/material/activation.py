"""This module contains a base class for activation models."""
from __future__ import division

__author__ = "Johan Hake (hake.dev@gmail.com), 2014"
__all__ = ["ActivationModel"]

import types
from collections import OrderedDict
from dolfin import Constant, Parameters, Expression, error, GenericFunction
import ufl

class ActivationModel(object):
    def __init__(self, params=None, init_conditions=None):
        """
        Create activation model (for now it can only be used in active
        stress models)

        *Arguments*
          time (Constant, optional)
            the time used to update an activation model
        """

        # Instantiate the parameters
        self._parameters = self.default_parameters()
        self._initial_conditions = self.default_initial_conditions()
        
        # Set parameters
        if params:
            assert isinstance(params, (Parameters, dict)), \
                   "expected a dict or a Parameters, as the modelparams argument"
            if isinstance(params, Parameters):
                params = params.to_dict()
            self.set_parameters(**params)

        if init_conditions:
            assert isinstance(init_conditions, dict), \
                "expected a dict or a Parameters, as the init_condition argument"
            if isinstance(init_conditions, Parameters):
                init_conditions = init_conditions.to_dict()
            self.set_initial_conditions(**init_conditions)

        # A flag to determine if a form using the activation model
        # need to be re initialized
        self.reinit_form = True
    
    def gamma(self, states=None, time=None, lambda_=None, lambda_dot=None):
        """Return the gamma expression

        *Arguments*
          states (dolfin.Function)
            A Function with the value of the present state variables needed
            to compute the gamma at a certain tim
          time (dolfin.Constant)
            A Constant with the present time
          lambda_ (dolfin.Function, ufl.Expr, optional)
            A Function or ufl.Expr determining the fiber strain 
          lambda_dot (dolfin.Function, ufl.Expr, optional)
            A Function or ufl.Expr determining the fiber strain rate 
        """
        gamma = self._parameters["gamma"]

        # Default values for arguments
        # FIXME: We can remove this as in this the most basic case as it
        # does not make any sense to 
        time = Constant(0.) if time is None else time
        lambda_ = Constant(1.0) if lambda_ is None else lambda_
        lambda_dot = Constant(0.) if lambda_dot is None else lambda_dot

        return gamma

    @staticmethod
    def default_parameters():
        "Set-up and return default model parameters. Should be overloaded in sub classes"
        return OrderedDict([("gamma", Constant(0.))])

    @staticmethod
    def default_initial_conditions():
        "Set-up and return default initial conditions. Should be overloaded in sub classes"
        return OrderedDict()

    def rhs(self, states):
        """Return the rhs expressions for the time dependent states
        determining the gamma method. Should be overloaded in sub
        classes."""
        return ufl.zero()

    def initial_conditions(self):
        "Return initial conditions for v and s as an Expression."
        return Expression(self._initial_conditions.keys(), \
                          **self._initial_conditions)

    def set_parameter(self, name, value):
        """Set a single parameter in the activation model
        """
        if name not in self._parameters:
            error("'%s' is not a parameter in %s" %(name, self))
        if not isinstance(value, (float, int, GenericFunction)):
            error("'%s' is not a scalar or a GenericFunction" % name)
        if isinstance(value, GenericFunction) and value.value_size() != 1:
            error("expected the value_size of '%s' to be 1" % name)

        # If setting parameters we will automatically change the
        # stored parameter following this scheme:
        # Old value type <- New value type : What we do
        # Float <- Float : Instantiate a Constant using new value
        # Float <- Constant : Use new value directly substituting old type
        # Float <- Expression : Use new value directly substituting old type
        # Float <- Function : Use new value directly substituting old type
        # Constant <- Float : Assign new float to old Constant
        # Constant <- Constant : Assign new Constant to old Constant
        # Constant <- Expression : Use new value directly substituting old type
        # Constant <- Function : Use new value directly substituting old type
        # Expression <- Float : Instantiate a Constant using new value
        # Expression <- Constant : Use new value directly substituting old type
        # Expression <- Expression : Use new value directly substituting old type
        # Expression <- Function : Use new value directly substituting old type
        # Function <- Float : Instantiate a Constant using new value
        # Function <- Constant : Use new value directly substituting old type
        # Function <- Expression : Use new value directly substituting old type
        # Function <- Function : Use new value directly substituting old type

        # New value is Float or Constant
        if isinstance(value, (float, int, Constant)):
            if isinstance(self._parameters[name], Constant):
                self._parameters[name].assign(value)
            else:
                self._parameters[name] = value
                self.reinit_form = True

        # New value is something else
        else:
            self._parameters[name] = value
            self.reinit_form = True

    def set_parameters(self, **params):
        """
        Update parameters in the activation model
        """
        for name, value in params.items():
            self.set_parameter(name, value)

    def set_initial_conditions(self, **params):
        "Update parameters in the activation model"
        for param_name, param_value in params.items():
            if param_name not in self._parameters:
                error("'%s' is not a parameter in %s" %(param_name, self))
            if not isinstance(param_value, (float, int, GenericFunction)):
                error("'%s' is not a scalar or a GenericFunction" % param_name)
            if isinstance(param_value, GenericFunction) and \
               param_value.value_size() != 1:
                error("expected the value_size of '%s' to be 1" % param_name)

            self._initial_conditions[param_name] = param_value

    def num_states(self):
        """Returns the number of states controlling the gamma
        expression. Should be overloaded in sub classes."""
        return 0
    
    def parameters(self):
        "Return the current parameters."
        return self._parameters

    def __str__(self):
        "Return string representation of activation model."
        return "Activation"

if __name__ == "__main__":
    act = ActivationModel()
    
