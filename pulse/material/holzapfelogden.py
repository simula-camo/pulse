from dolfin import *

from .activation import ActivationModel
# FIXME: Make a general Geometry class
from ..geometry.lvgeometry import LVGeometry

__all__ = ['HolzapfelOgden']

def subplus(x):
    return conditional(ge(x, 0.0), x, 0.0)

def heaviside(x):
    return conditional(ge(x, 0.0), 1.0, 0.0)

class HolzapfelOgden(object):
    """
    FIXME:
    """

    def __init__(self, params=None):

        params = params or {}
        self.parameters = self.default_parameters()
        self.parameters.update(params)

        # Default fibers, sheets and sheet normals
        # NOTE: These can be set after creation by a governing class
        self.f0 = None
        self.s0 = None
        self.n0 = None

    def select_material_parameters(param_set='wang_2013'):
        param_keys = [ "a_iso",    "b_iso",
                       "a_fibers", "b_fibers",
                       "a_sheets", "b_sheets",
                       "a_cross",  "b_cross" ]

        # parameters in kPa
        param_values = {
                "holzapfel_2009":
                [  0.059,  8.023,
                  18.472, 16.026,
                   2.481, 11.120,
                   0.216, 11.436 ],
                "goktepe_2011":
                [  0.496,  7.209,
                  15.192, 20.417,
                   3.283, 11.176,
                   0.662,  9.466 ],
                "wang_2013":
                [  0.2362, 10.810,
                  20.037,  14.154,
                   3.7245,  5.1645,
                   0.4108, 11.300 ],
                "simple_neohookean":
                [ 1.0, 0.0,
                  0.0, 0.0,
                  0.0, 0.0,
                  0.0, 0.0 ] }

        params = dict(zip(param_keys, param_values[param_set]))

        # kPa -> N/cm2
        for key in [ "iso", "fibers", "sheets", "cross" ]:
            params["a_%s" % key] /= 10.0

        self.parameters.update(**params)

    @staticmethod
    def default_parameters():
        # FIXME: Change to parameters ala ActivationModel

        # Geometry or problem can set, removing the nessesarity of
        # doing it between constructing the Geometry and instantiating
        # the material
        p = { 'active_model': 'active_strain',
              'use_fibers' : True,
              'use_sheets' : True,
              'a_iso'   : Constant(0.02362),
              'b_iso'   : Constant(10.810),
              'a_fibers': Constant(2.0037),
              'b_fibers': Constant(14.154),
              'a_sheets': Constant(0.37245),
              'b_sheets': Constant(5.1645),
              'a_cross' : Constant(0.04108),
              'b_cross' : Constant(11.300),
              }
        
        return p

    def is_incompressible(self):
        return True

    def is_isotropic(self):
        p = self.parameters
        anisop = ['a_fibers', 'a_sheets', 'a_cross']
        return sum(map(float, (p[k] for k in anisop))) < DOLFIN_EPS

    def W_1(self, I_1, diff=0):
        """
        Isotropic contribution.
        """
        a = self.parameters['a_iso']
        b = self.parameters['b_iso']

        if float(a) < DOLFIN_EPS:
            return 0
        elif float(b) < DOLFIN_EPS:
            if diff == 0:
                return a / 2.0 * (I_1 - 3)
            elif diff == 1:
                return a / 2.0
            elif diff == 2:
                return 0
        else:
            if diff == 0:
                return a/(2.0*b) * (exp(b*(I_1 - 3)) - 1)
            elif diff == 1:
                return a/2.0 * exp(b*(I_1 - 3))
            elif diff == 2:
                return a*b/2.0 * exp(b * (I_1 - 3))

    def W_4(self, I_4, param_set, diff=0):
        """
        Anisotropic contribution.
        """
        a = self.parameters['a_{}'.format(param_set)]
        b = self.parameters['b_{}'.format(param_set)]

        if I_4 == 0:
            return 0

        if float(a) < DOLFIN_EPS:
            return 0.0
        elif float(b) < DOLFIN_EPS:
            if diff == 0:
                return a/2.0 * heaviside(I_4 - 1.0) * pow(I_4 - 1.0, 2)
            elif diff == 1:
                return a * subplus(I_4 - 1)
            elif diff == 2:
                return heaviside(I_4 - 1)
        else:
            if diff == 0:
                return a/(2.0*b) * heaviside(I_4 - 1) * (exp(b*pow(I_4 - 1, 2)) - 1)
            elif diff == 1:
                return a * subplus(I_4 - 1) \
                         * exp(b * pow(I_4 - 1, 2))
            elif diff == 2:
                return a * heaviside(I_4 - 1) \
                         * (1 + 2.0 * b * pow(I_4 - 1, 2)) \
                         * exp(b * pow(I_4 - 1, 2))

    def W_8(self, I_8, diff=0):
        """
        Cross fiber-sheet contribution.
        """
        a = self.parameters['a_cross']
        b = self.parameters['b_cross']

        if I_8 == 0:
            return 0

        if float(a) < DOLFIN_EPS:
            return 0.0
        elif float(b) < DOLFIN_EPS:
            if diff == 0:
                return a / 2.0 * pow(I_8, 2)
            elif diff == 1:
                return a * I_8
            elif diff == 2:
                return a
        else:
            if diff == 0:
                return a/(2.0*b) * (exp(b * pow(I_8, 2)) - 1)
            elif diff == 1:
                return a * I_8 \
                         * exp(b * pow(I_8, 2))
            elif diff == 2:
                return a * (1 + 2.0 * b * pow(I_8, 2)) \
                         * exp(b * pow(I_8, 2))

    def strain_energy(self, F, p, gamma=None):
        """
        Total strain-energy density function.
        """

        # Usage of fibers or sheets
        use_fibers = self.f0 is not None and self.parameters['use_fibers']
        use_sheets = self.s0 is not None and self.parameters['use_sheets']

        C = F.T * F
        J = det(F)
        Jm23 = pow(J, -float(2)/3)

        # Invariants
        I1  = Jm23 * tr(C)

        I4f =  Jm23 * inner(C*self.f0, self.f0) if use_fibers else 0
        I4s =  Jm23 * inner(C*self.s0, self.s0) if use_sheets else 0
        I8fs = Jm23 * inner(C*self.f0, self.s0) if use_sheets and use_fibers else 0

        # Activation
        gamma = Constant(0) if gamma is None else gamma

        # Active stress model
        if self.parameters['active_model'] == 'active_stress':
            W1   = self.W_1(I1)
            W4f  = self.W_4(I4f, 'fibers')
            W4s  = self.W_4(I4s, 'sheets')
            W8fs = self.W_8(I8fs)
            Wactive = gamma * I4f
            #Wactive = gamma * 2./3. * sqrt(I4f**3)
            W = W1 + W4f + Wactive + W4s + W8fs

        # Active strain model
        elif self.parameters['active_model'] == 'active_strain':
            mgamma = 1 - gamma
            I1e   = mgamma * I1 + (1/mgamma**2 - mgamma) * I4f
            I4fe  = 1/mgamma**2 * I4f
            I4se  = mgamma * I4s
            I8fse = 1/sqrt(mgamma) * I8fs
            W1   = self.W_1(I1e)
            W4f  = self.W_4(I4fe, 'fibers')
            W4s  = self.W_4(I4se, 'sheets')
            W8fs = self.W_8(I8fse)
            W = W1 + W4f + W4s + W8fs

        return W - p * (J - 1)


