"""This module implements a single ventricular elipsoidal geometry
based on the prolate coordinate system
"""
# Copyright (C) 2014-2015 Simone Pezzuto
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.
#

import math as m
from textwrap import dedent

from .lvellipsoid import LVEllipsoid

class LVProlateEllipsoid(LVEllipsoid) :
    """
    A Prolate ellipsoid is defined through the prolate spheroidal
    coordinate system, which is:

        X_1 = d_focal cosh(Lambda) cos(M)
        X_2 = d_focal sinh(Lambda) sin(M) cos(Theta)
        X_3 = d_fcoal sinh(Lambda) sin(M) sin(Theta)

    The domain is defined as the volume included between the endocardium,
    the epicardium and the base, where :

        Gamma_endo = { Lambda = Lambda_endo,
                       -pi <= M <= Mu_base_endo, 0 <= Theta <= 2 pi }
        Gamma_epi  = { Lambda = Lambda_epi,
                       -pi <= M <= Mu_base_epi,  0 <= Theta <= 2 pi }
    
    The base is planar and orthogonal to X_1.
    """
    
    @classmethod
    def default_parameters(cls) :
        p = super(LVProlateEllipsoid, cls).default_parameters()
        p['geometry'] = \
            { 'd_focal' : 3.7,
              'l_endo'  : 0.4,
              'l_epi'   : 0.7,
              'mu_base' : 120.0 }
        return p

    def check_geometry_parameters(self) :
        """
        Check if the provided parameters are valid.
        """
        p = self._parameters['geometry']
        assert p['d_focal'] > 0.0
        assert 0.0 <= p['l_endo'] <= p['l_epi']
        assert - 180.0 <= p['mu_base'] <= 180.0

    def compute_endoring_offset(self) :
        """
        Position of the base (better, the endocardial ring).
        """
        d_focal = self._parameters['geometry']['d_focal']
        l_endo  = self._parameters['geometry']['l_endo']
        mu_base = self._parameters['geometry']['mu_base'] / 180.0 * m.pi
        return d_focal * m.cosh(l_endo) * m.cos(mu_base)

    def _compile_geo_code(self) :
        """
        Gmsh code for a prolate ellipsoid.
        """
        header = dedent(\
        """\
        r_short_endo = {d_focal} * Sinh({l_endo});
        r_short_epi  = {d_focal} * Sinh({l_epi});
        r_long_endo  = {d_focal} * Cosh({l_endo});
        r_long_epi   = {d_focal} * Cosh({l_epi});
        mu_base = {mu_base} / 180.0 * Pi;
        """).format(**self._parameters['geometry'])

        body = super(LVProlateEllipsoid, self)._compile_geo_body_code()

        return header + body

    def _compile_cart2coords_code(self) :
        p = self._parameters
        code = dedent(\
        """\
        #include <Eigen/Dense>

        class ProlateEllipsoidCart2Coords : public Expression
        {{
        public :

            std::shared_ptr<dolfin::GenericFunction> coords;

            ProlateEllipsoidCart2Coords() 
            : Expression(3)
            {{}}

            double solve_quadratic_pos_sol(double a, double b, double c) const
            {{
                b /= a;
                c /= a;
                double delta = std::sqrt(b*b - 4.0*c);
                double sol1 = (b > 0.) ? (-b - delta)/2.
                                       : (-b + delta)/2.;
                double sol2 = c/sol1;

                return (sol1 > 0.) ? sol1 : sol2;
            }}

            void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& raw_x,
                      const ufc::cell& cell) const
            {{
                // coordinate mapping
                const std::size_t value_size = {axisymmetric:^} ? 2 : 3;
                dolfin::Array<double> x_tmp(value_size);

                if (this->coords)
                {{
                    // high-order coordinate mapping
                    coords->eval(x_tmp, raw_x, cell);
                }}
                else
                {{
                    std::copy(raw_x.data(), raw_x.data() + value_size, x_tmp.data());
                }}

                dolfin::Array<double> x(3);
                x[0] = x_tmp[0];
                x[1] = x_tmp[1];

                if ({axisymmetric:^} == 1) x[2] = 0.0;
                else x[2] = x_tmp[2];

                // constants
                const double d_focal = {geometry[d_focal]};
                const double l_endo  = {geometry[l_endo]};
                const double l_epi   = {geometry[l_epi]};
                const double mu_base = {geometry[mu_base]} * M_PI / 180.0;

                // lambda
                double c0 = d_focal * d_focal;
                double c1 = - (x[0]*x[0] + x[1]*x[1] + x[2]*x[2] - c0);
                double c2 = - (x[1]*x[1] + x[2]*x[2]);

                double sinh2l = solve_quadratic_pos_sol(c0, c1, c2);
                double cosh2l = 1.0 + sinh2l;
                double sinhl = std::sqrt(sinh2l);
                double coshl = std::sqrt(cosh2l);
                values[0] = std::asinh(sinhl);

                // mu
                double a = std::sqrt(x[1]*x[1] + x[2]*x[2]) / sinhl;
                double b = x[0] / coshl;

                values[1] = std::atan2(a, b);

                // theta
                values[2] = (values[1] < DOLFIN_EPS)
                          ? 0.0
                          : M_PI - std::atan2(x[2], -x[1]);
            }}
        }};
        """).format(**self._parameters)

        return code

    def _compile_localbase_code(self) :
        p = self._parameters
        code = dedent(\
        """\
        #include <Eigen/Dense>

        class ProlateEllipsoidLocalCoords : public Expression
        {{
        public :

            typedef Eigen::Vector3d vec_type;
            typedef Eigen::Matrix3d mat_type;
            std::shared_ptr<dolfin::Expression> cart2coords;

            ProlateEllipsoidLocalCoords() : Expression(3, 3)
            {{}}

            void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& raw_x,
                      const ufc::cell& cell) const
            {{
                // check if coordinates are ok
                assert(this->cart2coords);

                // first find (lambda, mu, theta) from (x0, x1, x2)
                // axisymmetric case has theta = 0
                dolfin::Array<double> coords(3);
                this->cart2coords->eval(coords, raw_x, cell);

                double lambda = coords[0];
                double mu = coords[1];
                double theta = coords[2];

                // (e_1, e_2, e_3) = G (e_lambda, e_mu, e_theta)
                double d_focal = {geometry[d_focal]};
                double dsinh_l = d_focal * std::sinh(lambda);
                double dcosh_l = d_focal * std::cosh(lambda);
                double sin_m = std::sin(mu);
                double cos_m = std::cos(mu);
                double sin_t = std::sin(theta);
                double cos_t = std::cos(theta);

                mat_type base;
                base << dsinh_l*cos_m,       -dcosh_l*sin_m,        0.0,
                        dcosh_l*sin_m*cos_t,  dsinh_l*cos_m*cos_t, -dsinh_l*sin_m*sin_t,
                        dcosh_l*sin_m*sin_t,  dsinh_l*cos_m*sin_t,  dsinh_l*sin_m*cos_t;
                if (mu < DOLFIN_EPS)
                {{
                    // apex, e_mu and e_theta not defined
                    // --> random, but orthonormal
                    base << 1, 0, 0,
                            0, 1, 0,
                            0, 0, 1;
                }}
                base = base.colwise().normalized();

                Eigen::Map<mat_type>(values.data()) = base;
            }}
        }};
        """).format(**self._parameters)

        return code

    def _normalized_transmural_position_code(self) :
        code = dedent("""
        double l_endo = {l_endo};
        double l_epi  = {l_epi};
        pos = (coords[0] - l_endo) / (l_epi - l_endo);
        """).format(**self._parameters['geometry'])

        return code
