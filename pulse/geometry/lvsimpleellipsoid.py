"""This module implements a single ventricular elipsoidal geometry
based on the ellipsoidal coordinate system
"""
# Copyright (C) 2014-2015 Simone Pezzuto
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.

import math as m
from textwrap import dedent

from .lvprolateellipsoid import LVEllipsoid

class LVSimpleEllipsoid(LVEllipsoid) :
    """
    A simple ellipsoid is defined through the ellipsoidal
    coordinate system, which is:

        X_1 = r_long(t)  cos(M)
        X_2 = r_short(t) sin(M) cos(Theta)
        X_3 = r_short(t) sin(M) sin(Theta)

    for a particular choice of the radial functions :
        
        r_short(t) = r_short_endo . (1 - t) + r_short_epi . t
        r_long(t)  = r_long_endo  . (1 - t) + r_long_epi  . t
    
    The domain is defined as the volume included between the endocardium,
    the epicardium and the base, where :

       Gamma_endo = { t = 0, -pi <= M <= Mu_base_endo, 0 <= Theta <= 2 pi }
       Gamma_epi  = { t = 1, -pi <= M <= Mu_base_epi,  0 <= Theta <= 2 pi }
    
    The base is planar and orthogonal to X_1.

    Remark: the local base is *not* orthogonal, because e_t . e_mu != 0,
            so fiber and sheet defined in the usual way are not orthogonal
            as well.
    """
    
    @classmethod
    def default_parameters(cls) :
        p = super(LVSimpleEllipsoid, cls).default_parameters()
        p['geometry'] = \
            { 'r_short_endo' : 0.7,
              'r_short_epi'  : 1.0,
              'r_long_endo'  : 1.7,
              'r_long_epi'   : 2.0,
              'mu_base' : 120.0 }
        return p

    def check_geometry_parameters(self):
        """
        Check if the provided parameters are valid.
        """
        p = self._parameters['geometry']
        assert 0.0 <= p['r_short_endo'] <= p['r_short_epi']
        assert 0.0 <= p['r_long_endo'] <= p['r_long_epi']
        assert 0.0 <= 2.0 * p['r_short_endo'] - p['r_short_epi']
        assert 0.0 <= 2.0 * p['r_long_endo'] - p['r_long_epi']
        assert - 180.0 <= p['mu_base'] <= 180.0

    def compute_endoring_offset(self) :
        """
        Position of the base (better, the endocardial ring).
        """
        r_long_endo = self._parameters['geometry']['r_long_endo']
        mu_base = self._parameters['geometry']['mu_base'] / 180.0 * m.pi
        return r_long_endo * m.cos(mu_base)

    def _compile_geo_code(self) :
        """
        Gmsh code for a prolate ellipsoid.
        """
        header = dedent(\
        """\
        r_short_endo = {r_short_endo};
        r_short_epi  = {r_short_epi};
        r_long_endo  = {r_long_endo};
        r_long_epi   = {r_long_epi};
        mu_base = {mu_base} / 180.0 * Pi;
        """).format(**self._parameters['geometry'])

        body = super(LVSimpleEllipsoid, self)._compile_geo_body_code()

        return header + body

    def _compile_cart2coords_code(self) :
        code = dedent(\
        """\
        #include <boost/math/tools/roots.hpp>

        namespace dolfin
        {{

        using boost::math::tools::newton_raphson_iterate;

        class SimpleEllipsoidCart2Coords : public Expression
        {{
        public :

            std::shared_ptr<dolfin::GenericFunction> coords;

            SimpleEllipsoidCart2Coords() 
            : Expression(3)
            {{}}

            void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& raw_x,
                      const ufc::cell& cell) const
            {{
                // coordinate mapping
                const std::size_t value_size = {axisymmetric:^} ? 2 : 3;
                dolfin::Array<double> x_tmp(value_size);
                if (this->coords)
                {{
                    // high-order coordinate mapping
                    coords->eval(x_tmp, raw_x, cell);
                }}
                else
                {{
                    std::copy(raw_x.data(), raw_x.data() + value_size, x_tmp.data());
                }}

                dolfin::Array<double> x(3);
                x[0] = x_tmp[0];
                x[1] = x_tmp[1];

                if ({axisymmetric:^} == 1) x[2] = 0.0;
                else x[2] = x_tmp[2];
                
                // constants
                const double r_short_endo = {geometry[r_short_endo]};
                const double r_short_epi  = {geometry[r_short_epi]};
                const double r_long_endo  = {geometry[r_long_endo]};
                const double r_long_epi   = {geometry[r_long_epi]};

                // to find the transmural position we have to solve a
                // 4th order equation. It is easier to apply bisection
                // in the interval of interest [0, 1]
                auto fun = [&](double t)
                {{
                    double rs = r_short_endo + (r_short_epi - r_short_endo) * t;
                    double rl = r_long_endo + (r_long_epi - r_long_endo) * t;
                    double a2 = x[1]*x[1] + x[2]*x[2];
                    double b2 = x[0]*x[0];
                    double rs2 = rs*rs;
                    double rl2 = rl*rl;
                    double drs = (r_short_epi - r_short_endo) * t;
                    double drl = (r_long_epi - r_long_endo) * t;

                    double f  = a2 * rl2 + b2 * rs2 - rs2 * rl2;
                    double df = 2.0 * (a2 * rl * drl + b2 * rs * drs
                                - rs * drs * rl2 - rs2 * rl * drl);

                    return boost::math::make_tuple(f, df);
                }};

                int digits = std::numeric_limits<double>::digits;
                double t = newton_raphson_iterate(fun, 0.5, 0.0, 1.0, digits);
                values[0] = t;

                double r_short = r_short_endo * (1-t) + r_short_epi * t;
                double r_long  = r_long_endo  * (1-t) + r_long_epi  * t;

                double a = std::sqrt(x[1]*x[1] + x[2]*x[2]) / r_short;
                double b = x[0] / r_long;

                // mu
                values[1] = std::atan2(a, b);

                // theta
                values[2] = (values[1] < DOLFIN_EPS)
                          ? 0.0
                          : M_PI - std::atan2(x[2], -x[1]);
            }}
        }};

        }};
        """).format(**self._parameters)

        return code

    def _compile_localbase_code(self) :
        code = dedent(\
        """\
        #include <Eigen/Dense>

        namespace dolfin
        {{

        class SimpleEllipsoidLocalCoords : public Expression
        {{
        public :

            typedef Eigen::Vector3d vec_type;
            typedef Eigen::Matrix3d mat_type;
            std::shared_ptr<dolfin::Expression> cart2coords;

            SimpleEllipsoidLocalCoords() : Expression(3, 3)
            {{}}

            void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& raw_x,
                      const ufc::cell& cell) const
            {{
                // check if coordinates are ok
                assert(this->cart2coords);

                // first find (lambda, mu, theta) from (x0, x1, x2)
                // axisymmetric case has theta = 0
                dolfin::Array<double> coords(cart2coords->value_size());
                this->cart2coords->eval(coords, raw_x, cell);

                double t = coords[0];
                double mu = coords[1];
                double theta = coords[2];

                // (e_1, e_2, e_3) = G (e_t, e_mu, e_theta)
                const double r_short_endo = {geometry[r_short_endo]};
                const double r_short_epi  = {geometry[r_short_epi]};
                const double r_long_endo  = {geometry[r_long_endo]};
                const double r_long_epi   = {geometry[r_long_epi]};
                
                double rs = r_short_endo + (r_short_epi - r_short_endo) * t;
                double rl = r_long_endo + (r_long_epi - r_long_endo) * t;
                double drs = r_short_epi - r_short_endo;
                double drl = r_long_epi - r_long_endo;

                double sin_m = std::sin(mu);
                double cos_m = std::cos(mu);
                double sin_t = std::sin(theta);
                double cos_t = std::cos(theta);

                mat_type base;
                base << drl*cos_m,       -rl*sin_m,        0.0,
                        drs*sin_m*cos_t,  rs*cos_m*cos_t, -rs*sin_m*sin_t,
                        drs*sin_m*sin_t,  rs*cos_m*sin_t,  rs*sin_m*cos_t;
                if (mu < DOLFIN_EPS)
                {{
                    // apex, e_mu and e_theta not defined
                    // --> random, but orthonormal
                    base << 1, 0, 0,
                            0, 1, 0,
                            0, 0, 1;
                }}
                base = base.colwise().normalized();

                // in general this base is not orthonormal, unless
                //   d/dt ( rs^2(t) - rl^2(t) ) = 0
                bool enforce_orthonormal_base = true;
                if (enforce_orthonormal_base)
                {{
                    base.col(0) = base.col(1).cross(base.col(2));
                }}

                Eigen::Map<mat_type>(values.data()) = base;
            }}
        }};

        }};
        """).format(**self._parameters)

        return code

    def _normalized_transmural_position_code(self) :
        code = dedent("""
        pos = coords[0];
        """).format(**self._parameters['geometry'])

        return code
