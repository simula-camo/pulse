"""This module implements a base class for single ventricular
geometries
"""
# Copyright (C) 2014-2015 Simone Pezzuto
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from fenicshotools import save_geometry, load_geometry
import numpy as np

class LVGeometry(object):
    """
    Generic LV Geometry.

    Properties:
    - mesh
    - domain
    - bfun : facet mesh function
    - rfun : ridge mesh function
    - markers : dict of mesh markers
    - f0 : fiber field
    - s0 : sheet field
    - n0 : normal field
    """

    def __init__(self, domain, markers=None, f0=None, s0=None,
                       n0=None, long_axis='x'):

        assert isinstance(domain, (Mesh, Domain)), \
               "expected a Mesh of a Domain as the domain argument"

        # set up the mesh
        if isinstance(domain, Domain) :
            self.domain = domain
            self.mesh = domain.data()
        else:
            self.domain = domain.ufl_domain()
            self.mesh = domain
            
        # set up boundary markers
        log(PROGRESS, "LVGeometry: setting markers")
        dim = self.mesh.topology().dim()
        self.bfun = MeshFunction("size_t", self.mesh, dim-1, self.mesh.domains())
        self.rfun = MeshFunction("size_t", self.mesh, dim-2, self.mesh.domains())

        # set up the markers
        self.markers = markers or {}
        for v, (m, _) in self.markers.items():
            setattr(self, m, v)

        # Set up the microstructure. If not given they defaults to
        # None. If None material models will not include energies
        # related to these structures.
        self.f0 = f0
        self.s0 = s0
        self.n0 = n0

        # various 
        self._long_axis = {'x': 0, 'y': 1, 'z': 2}[long_axis]
        self._endoring_offset = self.compute_endoring_offset()

    @classmethod
    def from_file(cls, h5name, h5group='', comm=None):

        comm = comm if comm is not None else mpi_comm_world()

        return cls(*cls._load_from_file(comm, h5name, h5group))

    @staticmethod
    def _load_from_file(comm, h5name, h5group=''):
        # domain and markers
        log(PROGRESS, "LVGeometry: loading geometry from file")
        domain, markers = load_geometry(comm, h5name, h5group)

        # load the microstructure
        h5file = HDF5File(comm, h5name, 'r')
        fields = {}
        if h5file.has_dataset('{}/microstructure'.format(h5group)):
            for field in ['f0', 's0', 'n0']:
                fgroup = "{}/microstructure/{}".format(h5group, field)
                if not h5file.has_dataset(fgroup):
                    continue
                log(PROGRESS, "LVGeometry: loading '{}' from file".format(field))
                fspace = h5file.attributes(fgroup)['function_space']
                fname = h5file.attributes(fgroup)['name']
                family, degree = fspace.split("_")
                V = VectorFunctionSpace(domain, family, int(degree), 3)
                fun = Function(V, name=fname)
                h5file.read(fun, fgroup)
                fields[field] = fun
                
        h5file.close()

        f0, s0, n0 = fields.get('f0'), fields.get('s0'), fields.get('n0')

        return domain, markers, f0, s0, n0

    def save(self, h5name, h5group=''):
        # open the file
        comm = self.mesh.mpi_comm()
        save_geometry(comm, self.domain, h5name, h5group, self.markers)

        # Check if we have a UFL expression as a field. Then we need
        # to regenerate the microstructure and force interpolation
        fields = [self.f0, self.s0, self.n0]
        if all((field is not None) and (not isinstance(field, Function))\
               for field in fields):
            fields = self._generate_microstructure(self.domain, force_interp=True)
        
        # microstructure
        h5file = HDF5File(comm, h5name, 'a')
        for field in fields:

            # we can save only functions
            if not isinstance(field, Function):
                continue

            elm = field.function_space().ufl_element()
            family, order = elm.family(), elm.degree()
            fspace = '{}_{}'.format(family, order)
            fgroup = "{}/microstructure/{}".format(h5group, str(field))
            h5file.write(field, fgroup)
            h5file.attributes(fgroup)['function_space'] = fspace
            h5file.attributes(fgroup)['name'] = field.name()
            
        h5file.close()

    def compute_endoring_offset(self):
        ids = np.where(self.rfun.array() == self.ENDORING)[0]
        self.mesh.init(1, 0)
        pts = np.unique(np.hstack(map(self.mesh.topology()(1, 0), ids)))
        coords = self.mesh.coordinates()[pts, self._long_axis]
        quota_range = np.ptp(coords)
        quota_range = MPI.max(self.mesh.mpi_comm(), quota_range)

        if quota_range < DOLFIN_EPS:
            return coords[0]
        else:
            return None

    def inner_volume_form(self, u=None):
        # domain and boundaries
        dom = self.domain
        dim = dom.geometric_dimension()
        X = SpatialCoordinate(dom)
        N = FacetNormal(dom)

        # In general the base is not flat nor at quota = 0, so we need
        # a correction at least for the second case
        if self._endoring_offset is None:
            raise ValueError("The endoring at the base is not flat!")

        xshift = [ 0.0, 0.0, 0.0 ]
        xshift[self._long_axis] = self._endoring_offset
        xshift = Constant(tuple(xshift))

        u = u or Constant((0.0, 0.0, 0.0))

        x = X + u - xshift
        F = grad(x)
        n = cofac(F) * N

        return -1/float(dim) * inner(x, n)

    def surface_area_form(self, u=None):
        # domain and boundaries
        dom = self.domain
        dim = dom.geometric_dimension()
        X = SpatialCoordinate(dom)
        N = FacetNormal(dom)

        u = u or Constant((0.0, 0.0, 0.0))

        x = X + u
        F = grad(x)
        n = cofac(F) * N

        return sqrt(inner(n, n))

    def inner_volume(self, u=None, form_compiler_parameters=None):
        """
        Compute the inner volume of the cavity for a given displacement u
        """

        # Create integration measure providing a mesh function for the
        # endocardial domain
        # FIXME: Include logic for bi ventricular endocardial volum
        ds_endo = ds(self.ENDO, subdomain_data=self.bfun)

        Vendo_form = self.inner_volume_form(u) * ds_endo

        ffc_params = form_compiler_parameters or {}
        V = assemble(Vendo_form, form_compiler_parameters = ffc_params)
        return V

    def surface_area(self, surf, u=None, form_compiler_parameters=None):
        """
        Compute the surface area of a given exterior facet domain.
        """

        assert isinstance(surf, (int, str))

        if isinstance(surf, str):
            assert hasattr(self, surf)
            surf = getattr(self, surf)
            assert isinstance(surf, int)
            
        ds_endo = ds(surf, subdomain_data = self.bfun)

        area_form = self.surface_area_form(u) * ds_endo
        ffc_params = form_compiler_parameters or { 'quadrature_degree' : 4 }
        A = assemble(area_form, form_compiler_parameters = ffc_params)

        return A

