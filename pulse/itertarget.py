"""This module implements continuation algorithm to solve an LVProblem
to certain targets
"""
# Copyright (C) 2014-2015 Simone Pezzuto
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.
#

import operator as op
import numpy as np
from scipy.interpolate import splev, splrep

from dolfin import *
from lvsolver import LVSolver

__all__ = ["itertarget", "itertarget2"]

def itertarget(problem, target_end,
               data_collector=None,
               target_parameter="pressure",
               control_parameter="pressure", control_step=0.2,
               control_mode="pressure", tol=1e-6,
               adapt_step=True, max_adapt_iter=7):
    """
    Continuation of the control parameter by simple homotopy, i.e.
    the control is increased by control_step (and adapted) until the
    target is reached. The control mode selects which problem must
    be solved: pressure or volume driven.

    If the target is different from the control, then we solve the
    nonlinear equation

    .. math::

        g(control) = target_{end}

    where g is implicitely defined as the target value for a given
    control value.

    *Arguments*
      problem (:py:class:`pulse.LVProblem`)
        The problem containing the geometry and material laws
      data_collector (:py:class:`pulse.DataCollector`)
        An object to collect data during iterations
      target_end (float)
        The target value we are iterating towards
      target_parameters (str)
        The name of the target parameter
      control_step (float)
        The size of the step the control value will be changed each iteration
      control_parameter (str)
        The name of the control parameter
      control_mode (str)
        Either 'pressure' or 'volume'. If 'pressure' we set the cavity
        'pressure' and solve for the volume. If 'volume' we set the
        cavity volume and solve for the pressure.
      tol (float)
         Relative tolerance for reaching the target value.
      adapt_step (bool)
        If True the control step can be expanded if the previous
        iteration converged in less than 6 newton iterations.
    """

    # Initialize the nonlinear solver
    solver = LVSolver(use_snes=True)

    # Selects the control mode
    problem.set_control_mode(control_mode)

    # Check if the state is a solution
    solver.solve(problem)

    if data_collector:
        data_collector.save()
        data_collector.plot()
        data_collector.print_status()

    target_is_control = target_parameter == control_parameter

    # Sanity checks
    if control_parameter in [ "pressure", "volume" ] and \
       control_parameter != control_mode:
        error("control mode and control parameter have to be the same.")

    if not target_is_control:
        if target_parameter not in [ "volume", "pressure" ]:
            error("expected target_parameter to be one of volume "\
                  "or pressure if different from control parameter.")
        if target_parameter == control_mode:
            error("target parameter cannot be the same as the control mode.")

    # Get present value of both target and control parameters
    target_value  = problem.get_control_parameter(target_parameter)
    control_value = problem.get_control_parameter(control_parameter)

    # Get compare operator for checking when target value is reached (when control
    # and target is not the same parameter)
    comp = op.gt if target_value < target_end else op.lt

    info("\nITER TARGET {} {}".format(target_parameter, target_end))

    # Some flags
    target_reached = False

    # Used during prediction
    target_values  = [target_value]
    control_values = [control_value]
    prev_states = [problem.get_state().copy(True)]

    # The main loop
    iterating = False
    while not target_reached:

        first_step = len(target_values) < 2
        
        # Old values
        target_value_old  = target_values[-1]
        control_value_old = control_values[-1]
        state_old = prev_states[-1]

        # If target is control we simply increase control until we reach target
        if target_is_control:

            # If we are close to the target
            if comp(target_value + control_step, target_end):
                control_step = target_end - target_value

        # Else we need to iterate until target is passed and then find
        # correct control by interpolation.
        else:

            # Check if we cross the target
            # Interpolate!
            if not first_step:
                c0, c1 = control_values[-2:]
                t0, t1 = target_values[-2:]
                delta = (t1 - t0)/(c1 - c0)
                control_opt = 1/delta * (target_end - t0) + c0
                if iterating or abs(control_opt - c0) < abs(control_step):
                    
                    # If we have enough values for a spline 
                    if len(target_values) >= 4:
                        # Sort and create a spline representation of the
                        # values and use that for interpolation/extrapolation
                        inds = np.argsort(target_values)
                        tck = splrep(np.array(target_values)[inds],
                                     np.array(control_values)[inds], k=1, s=0)
                        
                        new_control = float(splev(target_end, tck))
                        
                        control_step = new_control - control_value

                    # If not we do a linear interpolation/extrapolation
                    else:
                        control_step = control_opt - c0
                        control_value = c0

                    iterating = True

        # New control value
        control_value += control_step

        # Prediction step
        # ---------------
        if not first_step:
            c0, c1 = control_values[-2:]
            s0, s1 = prev_states
            delta = (control_value - c0)/(c1 - c0)
            problem.get_state().vector().zero()
            problem.get_state().vector().axpy(1.0-delta, s0.vector())
            problem.get_state().vector().axpy(delta, s1.vector())
            #problem.get_state().vector().assign((1.0-delta)*s0 + delta*s1)

        # Correction step
        # ---------------
        info("\nTRYING NEW CONTROL VALUE: {}={}{}\ntarget: {}={}, "\
             "control parameter: \"{}\", control_mode: \"{}\"".format(
                 control_parameter, control_value, " (iterating)" if iterating else "",
                 target_parameter, target_end, control_parameter, control_mode))
        problem.set_control_parameters(**{control_parameter: control_value})
        try:
            nliter, nlconv = solver.solve(problem)
            if not nlconv:
                raise RuntimeError("Solver did not converge...")
        except RuntimeError, e:

            info("\nNOT CONVERGING")

            # Reset solution
            problem.get_state().vector().zero()
            problem.get_state().vector().axpy(1.0, state_old.vector())
            #problem.get_state().assign(state_old)
            control_value = control_value_old

            # Reduce step
            control_step *= 0.5
            info("REDUCING control_step = {}".format(control_step))
            continue

        # Get target value
        target_value = problem.get_control_parameter(target_parameter)

        # If we are going in the wrong direction
        if first_step and not iterating and abs(target_value - target_end) > \
               abs(target_value_old - target_end):

            info("\nSTEPING IN WRONG DIRECTION")
            
            # Reset solution
            problem.get_state().vector().zero()
            problem.get_state().vector().axpy(1.0, state_old.vector())
            #problem.get_state().assign(state_old)
            control_value = control_value_old
            
            # Reduce step
            #control_step *= -1
            # May happen that the control step is too large
            control_step *= -0.8

            continue

        # Adapt control_step
        if not iterating and nliter < max_adapt_iter and adapt_step:
            control_step *= 2.0
            info("\nINCREASING control_step = {}".format(control_step))

        # Check if target has been reached
        if abs(target_value - target_end) <= tol * target_end:
            target_str = "\nTARGET REACHED {} = {}".format(target_parameter, target_value)
            if target_parameter!=control_parameter:
                target_str += " WITH {} = {}".format(control_parameter, control_value)
            info(target_str)
            
            target_reached = True

        else:
            
            # Save state
            target_values.append(target_value)
            control_values.append(control_value)
            if first_step:
                prev_states.append(problem.get_state().copy(True))
            else:
                
                # Switch place of the state vectors
                prev_states = [prev_states[-1], prev_states[0]]

                # Inplace update of last state values
                prev_states[-1].vector().zero()
                prev_states[-1].vector().axpy(1.0, problem.get_state().vector())

            info("\nSUCCESFULL STEP:")

        # output
        if data_collector:
            if not iterating or target_reached:
                data_collector.save()
            data_collector.plot()
            data_collector.print_status()

    if data_collector:
        data_collector.flush()



def itertarget2(problem, target_end,
               data_collector=None,
               target_parameter="pressure",
               control_parameter="pressure", control_step=[0.2, 0.2],
               control_mode="pressure", tol=1e-6,
               adapt_step=True):
    """
    Continuation of the control parameter by simple homotopy, i.e.
    the control is increased by control_step (and adapted) until the
    target is reached. The control mode selects which problem must
    be solved: pressure or volume driven.

    If the target is different from the control, then we solve the
    nonlinear equation

    .. math::
    
      g(control) = target_{end}

    where g is implicitely defined as the target value for a given
    control value.

    *Arguments*
      problem (:py:class:`pulse.BiVProblem`)
        The problem containing the geometry and material laws
      data_collector (:py:class:`pulse.DataCollector`)
         An object to collect data during iterations
      target_end (array of floats)
         First idex is for lv and second is for rv
         The target value we are iterating towards
      target_parameters (str)
         The name of the target parameter
      control_step (array of floats)
         First idex is for lv and second is for rv
         The size of the step the control value will be changed each iteration
      control_parameter (str)
         The name of the control parameter
      control_mode (str)
         Either 'pressure' or 'volume'. If 'pressure' we set the cavity
         'pressure' and solve for the volume. If 'volume' we set the
         cavity volume and solve for the pressure.
      tol (float)
         Relative tolerance for reaching the target value.
      adapt_step (bool)
         If True the control step can be expanded if the previous
         iteration converged in less than 6 newton iterations.
    """

    # Initialize the nonlinear solver
    solver = LVSolver()

    # Selects the control mode
    problem.set_control_mode(control_mode)

    # Check if the state is a solution
    solver.solve(problem)

    if data_collector:
        data_collector.save()
        data_collector.plot()
        data_collector.print_status()

    target_is_control = target_parameter == control_parameter

    # Sanity checks
    if control_parameter in [ "pressure", "volume" ] and \
       control_parameter != control_mode:
        error("control mode and control parameter have to be the same.")

    if not target_is_control:
        if target_parameter not in [ "volume", "pressure" ]:
            error("expected target_parameter to be one of volume "\
                  "or pressure if different from control parameter.")
        if target_parameter == control_mode:
            error("target parameter cannot be the same as the control mode.")

    # Get present value of both target and control parameters
    target_value_lv = problem.get_control_parameter(target_parameter, vent = "LV")
    target_value_rv = problem.get_control_parameter(target_parameter, vent = "RV")

    if control_parameter in ["pressure", "volume"]:
        # Then we need to control both the left and right ventricle
        control_value_lv = problem.get_control_parameter(control_parameter, vent = "LV")
        control_value_rv = problem.get_control_parameter(control_parameter, vent = "RV")
    else:
        control_value = problem.get_control_parameter(control_parameter)



    # Get compare operator for checking when target value is reached (when control
    # and target is not the same parameter)
    comp_lv = op.gt if target_value_lv < target_end[0] else op.lt
    comp_rv = op.gt if target_value_rv < target_end[1] else op.lt

    info("\nITER TARGET {} LV: {} RV: {}".format(target_parameter, target_end[0], target_end[1]))

    # Some flags
    target_reached = False

    # used during prediction
    target_values_lv  = [ target_value_lv ]
    target_values_rv  = [ target_value_rv ]

    if control_parameter in ["pressure", "volume"]:
        control_values_lv = [ control_value_lv ]
        control_values_rv = [ control_value_rv ]

    else:
        control_values = [ control_value ]

    prev_states = [ problem.get_state().copy(True) ]

    # The main loop
    while not target_reached:

        first_step = len(target_values_lv) < 2
        iterating = False
        
        # Old values
        target_value_lv_old  = target_values_lv[-1]
        target_value_rv_old  = target_values_rv[-1]

        if control_parameter in ["pressure", "volume"]:
            control_value_lv_old = control_values_lv[-1]
            control_value_rv_old = control_values_rv[-1]
        else:
            control_value_old = control_values[-1]

        state_old = prev_states[-1]

        # If target is control we simply increase control until we reach target
        if target_is_control:

            # If we are close to the target
            if comp_lv(target_value_lv + control_step[0], target_end[0]):
                control_step[0] = target_end[0] - target_value_lv
            if comp_rv(target_value_rv + control_step[1], target_end[1]):
                control_step[1] = target_end[1] - target_value_rv

        # Else we need to iterate until target is passed and then find
        # correct control by interpolation.
	########  NEED TO OPTIMIZE LATER FOR BiV. FOR NOW JUST IGNORE ##########
        # else:

            # check if we cross the target
        #    if not first_step:
        #        c0, c1 = control_values[-2:]
        #        t0, t1 = target_values[-2:]
        #        delta = (t1 - t0)/(c1 - c0)
        #        control_opt = 1/delta * (target_end - t0) + c0
        #        if iterating or abs(control_opt - c0) < abs(control_step):
                    
                    # If first time add old values
                    #kind = {2:"linear", 3:"slinear", 4:"quadratic", 5:"cubic"}.get(\
                    #    len(control_values), "cubic")
                    
                    ## Sort and interpolate control value for target_end
                    #inds = np.argsort(target_values)
                    #new_control = float(interp.interp1d(\
                    #    np.array(target_values)[inds],
                    #    np.array(control_values)[inds], kind)(target_end))
                    
                    #control_step = new_control - control_value
                    #iterating = True

        #            iterating = True
        #            control_step = control_opt - c0
        #            control_value = c0

        # New control value
        if control_parameter in ["pressure", "volume"]:
            control_value_lv += control_step[0]
            control_value_rv += control_step[1]
        else:
            control_value += control_step

        # Prediction step
        # ---------------
        if not first_step:
            if control_parameter in ["pressure", "volume"]:
                c0_lv, c1_lv = control_values_lv[-2:]
                c0_rv, c1_rv = control_values_rv[-2:]
                delta_lv = (control_value_lv - c0_lv)/(c1_lv - c0_lv)
                delta_rv = (control_value_rv - c0_rv)/(c1_rv - c0_rv)
                delta = 0.5*(delta_lv+delta_rv)
            else:
                c0, c1 = control_values[-2:]
                delta = (control_value - c0)/(c1 - c0)
            s0, s1 = prev_states
            
            problem.get_state().vector().zero()
            problem.get_state().vector().axpy(1.0-delta, s0.vector())
            problem.get_state().vector().axpy(delta, s1.vector())
            #problem.get_state().vector().assign((1.0-delta)*s0 + delta*s1)

        # Correction step
        # ---------------
        if control_parameter in ["pressure", "volume"]:
            info("\nTRYING NEW CONTROL VALUE: {} LV={}, RV={}\ntarget: {} LV={}, RV={}, \n"\
                 "control parameter: \"{}\", control_mode: \"{}\"".format(
                     control_parameter, control_value_lv, control_value_rv, target_parameter, \
                     target_end[0], target_end[1], control_parameter, control_mode))
            problem.set_control_parameters(**{control_parameter: control_value_lv, "vent": "LV"})
            problem.set_control_parameters(**{control_parameter: control_value_rv, "vent": "RV"})
        else:
            info("\nTRYING NEW CONTROL VALUE: {}={}\ntarget: {}={}, "\
                 "control parameter: \"{}\", control_mode: \"{}\"".format(
                     control_parameter, control_value, target_parameter, \
                     target_end, control_parameter, control_mode))
            problem.set_control_parameters(**{control_parameter: control_value})
        try:
            nliter, nlconv = solver.solve(problem)
        except RuntimeError:

            info("\nNOT CONVERGING")

            # Reset solution
            problem.get_state().vector().zero()
            problem.get_state().vector().axpy(1.0, state_old.vector())
            #problem.get_state().assign(state_old)
            if control_parameter in ["pressure", "volume"]:
                control_value_lv = control_value_lv_old
                control_value_rv = control_value_rv_old
                control_step = [i*0.5 for i in control_step]
            else:
                control_value = control_value_old
		control_step *= 0.5

            info("REDUCING control_step = {}".format(control_step))
            continue

	
        # Get target value
        target_value = problem.get_control_parameter(target_parameter)

        # If we are going in the wrong direction
	if control_parameter in ["pressure", "volume"]:
            target_value_lv = problem.get_control_parameter(target_parameter, vent="LV")
            target_value_rv = problem.get_control_parameter(target_parameter, vent="RV")

            if first_step and not iterating and abs(target_value_lv - target_end[0]) > abs(target_value_lv_old - target_end[0]):
            
                info("\nSTEPING IN WRONG DIRECTION for LV {}".format(target_parameter))
                control_value_lv = 0.8*control_value_lv_old
                
                # Revert step
                control_step[0] *= -1
            
                # Reset solution
                problem.get_state().vector().zero()
                problem.get_state().vector().axpy(1.0, state_old.vector())
                #problem.state.assign(state_old)
                
            if first_step and not iterating and abs(target_value_rv - target_end[1]) > abs(target_value_rv_old - target_end[1]):
            
                info("\nSTEPING IN WRONG DIRECTION for RV {}".format(target_parameter))
                control_value_rv = 0.8*control_value_rv_old
                
                # Revert step
                control_step[1] *= -1
            
                # Reset solution
                problem.get_state().vector().zero()
                problem.get_state().vector().axpy(1.0, state_old.vector())
                #problem.state.assign(state_old)

    
        else:
            target_value = problem.get_control_parameter(target_parameter)

            # If we are going in the wrong direction
            if first_step and not iterating and (abs(target_value_lv - target_end[0]) > abs(target_value_lv_old - target_end[0]) or abs(target_value_rv - target_end[1]) > abs(target_value_rv_old - target_end[1])):
            
                info("\nSTEPING IN WRONG DIRECTION")
            
                # Reset solution
                problem.get_state().vector().zero()
                problem.get_state().vector().axpy(1.0, state_old.vector())
                #problem.state.assign(state_old)
                control_value = control_value_old
                
                # Reduce step
                control_step *= -1
            
                continue


        # Adapt control_step
        if not iterating and nliter < 7 and adapt_step:
            if control_parameter in ["pressure", "volume"]:
                control_step = [2*i for i in control_step]
		info("\nINCREASING control_step = {}".format(control_step[0]))
            else:
                control_step *= 2.0
            	info("\nINCREASING control_step = {}".format(control_step))

        # output
        if data_collector:
            data_collector.save()
            data_collector.plot()
            data_collector.print_status()

        # Check if target has been reached
        if abs(target_value_lv - target_end[0]) <= tol * target_end[0] and abs(target_value_rv - target_end[1]) <= tol * target_end[1]:
            target_str = "\nTARGET REACHED for LV {} = {} and RV {} = {}".format(target_parameter, target_value_lv, target_parameter, target_value_rv)
            if target_parameter!=control_parameter:
                target_str += " WITH {} = {} for LV and {}={} for RV".format(control_parameter, control_value_lv, control_parameter, control_value_rv)
            info(target_str)
            
            target_reached = True

        else:
            
            # Save state
            target_values_lv.append(target_value_lv)
            target_values_rv.append(target_value_rv)

            if control_parameter in ["pressure", "volume"]:
                control_values_lv.append(control_value_lv)
                control_values_rv.append(control_value_rv)
            else:
                control_values.append(control_value)
            if first_step:
                prev_states.append(problem.get_state().copy(True))
            else:
                
                # Switch place of the state vectors
                prev_states = [prev_states[-1], prev_states[0]]

                # Inplace update of last state values
                prev_states[-1].vector().zero()
                prev_states[-1].vector().axpy(1.0, problem.get_state().vector())

            info("\nSUCCESFULL STEP:")

    if data_collector:
        data_collector.flush()


