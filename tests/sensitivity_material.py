from dolfin import *
from pulse.lvproblem import LVProblem
from pulse.itertarget import itertarget

from pulse.geometry import LVProlateEllipsoid
from pulse.material import HolzapfelOgden

import numpy as np
import matplotlib.pyplot as plt

parameters['form_compiler']['representation'] = 'uflacs'

def tmp_proc(problem) :
    pass

def get_apex_position(problem, surf = 'endo') :
    u = problem.get_state().split(True)[0]
    return problem.geo.get_apex_position(surf, u)

def passive_inflation(geo, mat, pend=0.1, pstep=0.01) :

    # problem
    param = LVProblem.default_parameters()
    param["bc_type"] = 'fix_base_ver'
    param["geometry"] = geo
    param["material"] = mat
    problem = LVProblem(**param)

    # passive inflation
    itertarget(problem,
               target_end = pend,
               target_parameter = "pressure", control_step = pstep,
               control_parameter = "pressure", control_mode = "pressure")

    V = problem.get_Vendo()
    l = get_apex_position(problem, 'epi')
    apexthick = l - get_apex_position(problem, 'endo')

    return V, l, apexthick

if __name__ == "__main__" :

    comm = mpi_comm_world()

    # geometry
    meshname = "./results/simple_pvloop/mesh.h5"
    geoparam = LVProlateEllipsoid.default_parameters()
    geoparam['mesh_generation']['ndiv'] = 1
    geoparam['mesh_generation']['order'] = 2
    geoparam['axisymmetric'] = True
    geo = LVProlateEllipsoid(meshname, "", comm, True, geoparam)

    # material
    res = []
    aiso = np.linspace(1.0, 30.0, num=50)
    af   = np.linspace(1.0, 20.0, num=50)
    AISO, AF = np.meshgrid(aiso, af)

    VOL = np.zeros(AISO.shape)
    LENGTH = np.zeros(AISO.shape)
    ATHICK = np.zeros(AISO.shape)

    for i in xrange(VOL.shape[0]) :
        for j in xrange(VOL.shape[1]) :
            mat = HolzapfelOgden()
            matparam = HolzapfelOgden.default_parameters()
            matparam['f0'] = geo.f0
            matparam['s0'] = geo.s0
            matparam['b_iso'] = Constant(AISO[i,j])
            matparam['b_fibers'] = Constant(AF[i,j])
            mat = HolzapfelOgden(**matparam)
            VOL[i,j], LENGTH[i,j], ATHICK[i,j] = passive_inflation(geo, mat)

    # plot
    fig, axs = plt.subplots(1,3)
    fig.set_size_inches(16., 4.)
    for v, l, ax in zip([VOL, LENGTH, ATHICK], ['V_EDP', 'Length', 'Thickness'], axs.ravel()) :
        CS = ax.contourf(AISO, AF, v, 50)
        ax.set_title('Sensitivity for {}'.format(l))
        ax.set_xlabel('b_iso')
        ax.set_ylabel('b_f')
        cbar = fig.colorbar(CS, ax=ax, shrink=0.8)
        #cbar.ax.set_ylabel(l)
    plt.subplots_adjust(left=0.04, right=0.99, bottom=0.13, top=0.9, wspace=0.14)
    #plt.show()
    fig.savefig('sensitivity.png', dpi=300)
