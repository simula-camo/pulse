from dolfin import *
from pulse.geometry import LVProlateEllipsoid
from pulse.geometry import LVSimpleEllipsoid
from pulse.geometry import LVGeometry

import os.path

parameters['form_compiler']['representation'] = 'uflacs'

def test_geotype(cls) :
    name = cls.__name__
    meshname = "{}.h5".format(name)
    coordname = "{}_coords.xdmf".format(name)
    microname = "{}_micro.xdmf".format(name)
    lbasename = "{}_localbase.xdmf".format(name)

    regen = not os.path.isfile(meshname)

    param = cls.default_parameters()
    param['mesh_generation']['ndiv'] = 4
    param['mesh_generation']['order'] = 2
    param['axisymmetric'] = True
    param['microstructure']['interpolate_at_nodes'] = True
    param['microstructure']['analytic_expression'] = True
    geo = cls(meshname, "", mpi_comm_world(), regen, param)

    print "Volume({}) = {}".format(cls.__name__, geo.inner_volume())

    Vv = VectorFunctionSpace(geo.domain, "P", 1, 3)
    Vt = TensorFunctionSpace(geo.domain, "P", 1, shape = (3, 3))
    
    coords = Expression(cppcode = geo._compile_cart2coords_code())
    File(coordname) << interpolate(coords, Vv)

    localbase = Expression(cppcode = geo._compile_localbase_code())
    localbase.cart2coords = coords
    File(lbasename) << interpolate(localbase, Vt)

    micro = geo.f0.operands()[0].operands()[0]
    File(microname) << interpolate(micro, Vt)

if __name__ == "__main__" :
    test_geotype(LVProlateEllipsoid)
    test_geotype(LVSimpleEllipsoid)
