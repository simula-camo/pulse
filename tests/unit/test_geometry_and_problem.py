import random
import shutil
import math as m
from dolfin import *
from pulse import *

import os.path
import numpy as np
import pytest
import itertools

parametrize = pytest.mark.parametrize

ellipsoid_types = ["prolate", "ellipsoid"]
ndivs = [2,3]
orders = [1,2]
axisymetrics = [False, True]
interpolates = [True, False]
mtrl_types = ["holzapfelogden"]
bc_types = ["fix_base", "fix_base_ver", "fix_epi_area", "free"]

def create_geometry(ellipsoid_type, ndiv, order, axisymetric, interpolate, tag, \
                    save_mesh=True, params=None):
    """
    Creates a geometry with a given set of parameters.
    """
    assert ellipsoid_type in ellipsoid_types
    assert ndiv in ndivs
    assert order in orders
    assert isinstance(axisymetric, bool)
    assert isinstance(interpolate, bool)

    params = params or {}

    # Pick ellipsoid class
    if ellipsoid_type == "prolate":
        LVEllipsoid = LVProlateEllipsoid
    else:
        LVEllipsoid = LVSimpleEllipsoid

    geoparam = LVEllipsoid.default_parameters()
    geoparam['mesh_generation']['ndiv'] = ndiv
    geoparam['mesh_generation']['order'] = order
    geoparam['axisymmetric'] = axisymetric
    geoparam['microstructure']['interpolate'] = interpolate
    geoparam["geometry"].update(params)

    meshname = "./data/mesh_{}_{}_{}_{}_{}_{}.h5".format(\
        ellipsoid_type, ndiv, order, int(axisymetric), int(interpolate), tag) \
        if save_mesh else ""

    # Check if we should regen the mesh
    regen = not os.path.isfile(meshname)
    if not regen or not save_mesh:

        geom = LVEllipsoid(meshname, "", mpi_comm_world(), regen, geoparam)
        
    else:
        
        # Safeguard for threaded runs
        tmp_meshname = meshname.replace("mesh_", "mesh_%016x_" % random.getrandbits(64))
        geom = LVEllipsoid(tmp_meshname, "", mpi_comm_world(), regen, geoparam)
        shutil.move(tmp_meshname, meshname)
        
    return geom

def create_material(mtrl_type):

    assert mtrl_type in ["holzapfelogden"]

    MaterialModel = HolzapfelOgden
    params = MaterialModel.default_parameters()

    return MaterialModel(params)

def create_problem(ellipsoid_type, ndiv, order, axisymetric, mtrl_type, \
                   bc_type, interpolate):

    # Create geometry
    geo = create_geometry(ellipsoid_type, ndiv, order, axisymetric, interpolate, "problem")

    # Create Material
    mat = create_material(mtrl_type)
    
    assert bc_type in ["fix_base", "fix_base_ver", "fix_epi_area", "free"]
    if bc_type == "fix_epi_area":
        assert geo.is_axisymmetric

    params = LVProblem.default_parameters()
    params["bc_type"] = bc_type

    return LVProblem(geo, mat, params=params)

@parametrize(("ellipsoid_type", "ndiv", "order", "axisymetric"),
             list(itertools.product(ellipsoid_types, ndivs, orders, \
                                    axisymetrics)))
def test_geom_construction(ellipsoid_type, ndiv, order, axisymetric):
    if ellipsoid_type == "ellipsoid":
        params = {}
        prolate_params = LVProlateEllipsoid.default_parameters()["geometry"]
    
        params["r_short_endo"] = prolate_params["d_focal"]*\
                                 m.sinh(prolate_params["l_endo"])
        params["r_short_epi"] = prolate_params["d_focal"]*\
                                m.sinh(prolate_params["l_epi"])
        params["r_long_endo"] = prolate_params["d_focal"]*\
                                m.cosh(prolate_params["l_endo"])
        params["r_long_epi"] = prolate_params["d_focal"]*\
                               m.cosh(prolate_params["l_epi"])
    else:
        params = {}
    
    geom = create_geometry(ellipsoid_type, ndiv, order, axisymetric, \
                           False, "geometry", params=params)

    epi_area = 106.6
    endo_area = 49.9
    base_area = 14.7
    inner_volume = 32.5

    assert abs(geom.surface_area("EPI")-epi_area)<0.25
    assert abs(geom.surface_area("ENDO")-endo_area)<0.2
    assert abs(geom.surface_area("BASE")-base_area)<0.2
    assert abs(geom.inner_volume()-inner_volume)<0.35

@parametrize(("ellipsoid_type", "ndiv", "order", "axisymetric", "mtrl_type", \
              "bc_type", "interpolate"),
             list(itertools.product(ellipsoid_types, ndivs, orders, \
                                    axisymetrics, mtrl_types, bc_types, interpolates)))
def test_lv_problem_construction(ellipsoid_type, ndiv, order, axisymetric, \
                                 mtrl_type, bc_type, interpolate):
    
    if bc_type in ["fix_epi_area", "free"] and not axisymetric:
        return

    problem = create_problem(ellipsoid_type, ndiv, order, axisymetric, \
                             mtrl_type, bc_type, interpolate)

@parametrize(("ndiv", "order", "axisymetric"),
             list(itertools.product(ndivs, orders, axisymetrics)))
def test_prolate_vs_simple(ndiv, order, axisymetric):
    prolate_params = LVProlateEllipsoid.default_parameters()["geometry"]
    ellipsoid_params  = LVSimpleEllipsoid.default_parameters()["geometry"]
    
    ellipsoid_params["r_short_endo"] = prolate_params["d_focal"]*\
                                       m.sinh(prolate_params["l_endo"])
    ellipsoid_params["r_short_epi"] = prolate_params["d_focal"]*\
                                       m.sinh(prolate_params["l_epi"])
    ellipsoid_params["r_long_endo"] = prolate_params["d_focal"]*\
                                       m.cosh(prolate_params["l_endo"])
    ellipsoid_params["r_long_epi"] = prolate_params["d_focal"]*\
                                       m.cosh(prolate_params["l_epi"])
    
    prol_geom = create_geometry("prolate", ndiv, order, axisymetric, False, \
                                "dummy", save_mesh=False, params=prolate_params)
    ellipsoid_geom = create_geometry("ellipsoid", ndiv, order, axisymetric, False, \
                                     "dummy", save_mesh=False, params=ellipsoid_params)

    assert abs(prol_geom.surface_area("EPI") - ellipsoid_geom.surface_area("EPI"))<0.01
    assert abs(prol_geom.surface_area("ENDO") - ellipsoid_geom.surface_area("ENDO"))<0.01
    assert abs(prol_geom.surface_area("BASE") - ellipsoid_geom.surface_area("BASE"))<0.01
    assert abs(ellipsoid_geom.inner_volume() - prol_geom.inner_volume())<0.01
    
@parametrize(("ellipsoid_type", "ndiv", "order", "axisymetric", "mtrl_type", \
              "bc_type", "interpolate"),
             list(itertools.product(ellipsoid_types, ndivs, orders, \
                                    axisymetrics, mtrl_types, bc_types, interpolates)))
def test_simple_pvloop(ellipsoid_type, ndiv, order, axisymetric, \
                       mtrl_type, bc_type, interpolate):
    
    if bc_type in ["fix_epi_area", "free"] and not axisymetric:
        return

    p_atrium = 0.1
    p_aortic = 1.0
    gamma_max = 0.2

    # Scalar values for the different stages
    V = [32.5, 39.5, 39.5, 16.8, 16.8, 39.5]
    A_epi = [106.6, 111.7, 112,1, 97.6, 97.5, 111.7]
    gamma = [0., 0., 0.0802, 0.2, 0.16, 0.]
    vol_tol = 0.55
    
    # Params
    #ellipsoid_type = "prolate"
    #ndiv = 2
    #order = 1
    #axisymetric = False
    #mtrl_type = "holzapfelogden"
    #bc_type = "fix_base"
    #interpolate = True
    adapt = True
    
    problem = create_problem(ellipsoid_type, ndiv, order, axisymetric, \
                             mtrl_type, bc_type, interpolate)

    problem._parameters['always_recompute_jacobian'] = False
    collector = DataCollector("./results", problem)

    assert abs(V[0]-problem.get_Vendo())<=vol_tol
    info("VOLUME = {}".format(problem.get_Vendo()))
    info("AREA = {}".format(problem.get_epiarea()))

    # passive inflation
    itertarget(problem, target_end=p_atrium, target_parameter="pressure",
               control_step=0.02, control_parameter="pressure",
               control_mode="pressure", data_collector=collector, adapt_step=adapt)

    assert abs(V[1]-problem.get_Vendo())<=vol_tol
    info("VOLUME = {}".format(problem.get_Vendo()))
    info("AREA = {}".format(problem.get_epiarea()))

    # isovolumic contraction
    itertarget(problem, target_end=p_aortic, target_parameter="pressure",
               control_step=0.01, control_parameter="gamma", control_mode="volume",
               data_collector=collector, adapt_step=adapt)

    assert abs(V[2]-problem.get_Vendo())<=vol_tol
    info("VOLUME = {}".format(problem.get_Vendo()))
    info("AREA = {}".format(problem.get_epiarea()))

    # isotonic contraction
    itertarget(problem, target_end=gamma_max, target_parameter="gamma",
               control_step=0.01, control_parameter="gamma", control_mode="pressure",
               data_collector=collector, adapt_step=adapt)

    assert abs(V[3]-problem.get_Vendo())<=vol_tol
    info("VOLUME = {}".format(problem.get_Vendo()))
    info("AREA = {}".format(problem.get_epiarea()))

    # isovolumic relaxation
    itertarget(problem, target_end=p_atrium, target_parameter="pressure",
               control_step=-0.01, control_parameter="gamma",
               control_mode="volume", data_collector=collector, adapt_step=adapt)


    assert abs(V[4]-problem.get_Vendo())<=vol_tol
    info("VOLUME = {}".format(problem.get_Vendo()))
    info("AREA = {}".format(problem.get_epiarea()))

    # isotonic relaxation
    itertarget(problem, target_end=0.0, target_parameter="gamma", control_step=-0.01,
               control_parameter="gamma", control_mode="pressure",
               data_collector=collector, adapt_step=adapt)

    assert abs(V[5]-problem.get_Vendo())<=vol_tol
    info("VOLUME = {}".format(problem.get_Vendo()))
    info("AREA = {}".format(problem.get_epiarea()))

if __name__ == "__main__":

    if mpi_comm_world().rank == 0:
        set_log_level(INFO)
    else:
        set_log_level(ERROR)
    
    # iterations
    #simple_pvloop(problem, p_atrium = 0.1, p_aortic = 1.0, gamma_max = 0.2)



