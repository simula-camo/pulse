function dy = lumped_heart(t, y, f_act)

    % 0 * dl/dt = f_1(lf, ls, p, V)
    % 0 * dl/dt = f_2(lf, ls, p, V)
    % 0 * dp/dt = f_3(lf, ls, p, V)
    % dV/dt - C * dp/dt = g(p, V)
    % df_act/dt = fun(lf, f_act)
    
    % state
    lambda_f = y(1);
    lambda_s = y(2);
    p_fluid  = y(3);
    V_fluid  = y(4);
    f_act    = y(5);

    % active force (from electrophysiology)
    %f_act = 0.5 * subplus(-sin(0.5*pi*t));
    dy(5) = fun(lambda_f, f_act);
    
    dy = zeros(4, 1);
    
    % mechanical model
    dy(1:3) = simple_heart(lambda_f, lambda_s, p_fluid, V_fluid, f_act);
    
    % circulation model
    p_atrium = 0.12;
    p_aorta  = 0.4;
    k_mitral = 10.0;
    k_aorta  = 0.8;
 
    dy(4) = k_mitral * subplus(p_atrium - p_fluid) ...
          - k_aorta  * subplus(p_fluid - p_aorta);
 
end
