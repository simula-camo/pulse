function dy = lumped_heart_actstrain(t, y)

    % 0 * dl/dt = f_1(l1, l2, p, V)
    % 0 * dl/dt = f_2(l1, l2, p, V)
    % 0 * dp/dt = f_3(l1, l2, p, V)
    % dV/dt - C * dp/dt = g(p, V)
    
    % state
    lambda_f = y(1);
    lambda_s = y(2);
    p_fluid  = y(3);
    V_fluid  = y(4);

    % activation (from electrophysiology)
    gamma = 0.15 * subplus(-sin(0.5*pi*t));
    
    dy = zeros(4, 1);
    
    % mechanical model
    dy(1:3) = simple_heart_actstrain(lambda_f, lambda_s, p_fluid, V_fluid, gamma);
    
    % circulation model
    p_atrium = 0.12;
    p_aorta  = 0.4;
    k_mitral = 10.0;
    k_aorta  = 0.8;
 
    dy(4) = k_mitral * subplus(p_atrium - p_fluid) ...
          - k_aorta  * subplus(p_fluid - p_aorta);
 
end
