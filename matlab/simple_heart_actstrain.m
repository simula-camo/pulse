function res = simple_heart_actstrain(lambda_f, lambda_s, p_fluid, ...
                                      V_fluid, gamma_f)

    % material parameters (from Wang et al. (2013))
    % 1 N/cm2 = 10 kPa
    a_iso =  0.02362;
    b_iso = 10.810;
    a_f   =  2.0037;
    b_f   = 14.154;
    a_s   =  0.37245;
    b_s   =  5.1645;

    % microstructure
    f0 = [ 1; 0; 0 ];
    s0 = [ 0; 1; 0 ];
    n0 = [ 0; 0; 1 ];

    % active deformation
    gamma_s = 1 - 1/sqrt(1-gamma_f);
    gamma_n = 1 - 1/sqrt(1-gamma_f);
    
    Fa = (1 - gamma_f) * (f0 * f0.') + ...
         (1 - gamma_s) * (s0 * s0.') + ...
         (1 - gamma_n) * (n0 * n0.');
    
    % total deformation
    lambda_n = 1/(lambda_f*lambda_s);
    
    F = lambda_f * (f0 * f0.') + ...
        lambda_s * (s0 * s0.') + ...
        lambda_n * (n0 * n0.');

    % passive deformation
    Fe = F * inv(Fa);
    Ce = Fe.' * Fe;
    Be = Fe * Fe.';
    
    % invariants
    Ie1  = trace(Ce);
    Ie4f = dot(Ce*f0, f0);
    Ie4s = dot(Ce*s0, s0);
    
    % strain-energy derivatives
    dW1  = a_iso/2 * exp(b_iso*(Ie1 - 3));
    dW4f = a_f * subplus(Ie4f - 1) * exp(b_f*(Ie4f - 1)^2);
    dW4s = a_s * subplus(Ie4s - 1) * exp(b_s*(Ie4s - 1)^2);
    
    % residual
    res = zeros(3, 1);
        
    % passive cauchy stress
    Tpas = 2.0 * dW1 * Be ...
         + 2.0 * dW4f * ((Be*f0) * f0.') ...
         + 2.0 * dW4s * ((Be*s0) * s0.');
    
    % reaction constraint for fluid
    Text = p_fluid * (det(F)*(F.'\s0) * s0.');
    
    % reaction constraint for incompressibility
    % assumption: n0 direction stress-free
    Tinc = - dot(Tpas*n0, n0)*eye(3);
    
    % stress balance
    T = Tpas + Tinc + Text;
    
    % since n0 direction is stress-free, balance is given by
    res(1) = dot(T*f0, f0);
    res(2) = dot(T*s0, s0);

    % volume constraint (in cm3)
    A0 = 1.0; l0 = 1.0;
    V_ref = 1.0;
    res(3) = (1 - lambda_s)*A0*l0 + V_ref - V_fluid;
    
end
