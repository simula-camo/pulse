// =====
// specs
// -----
// dfocal  = 4.5; // [cm]
// l_endo  = 0.6;
// l_epi   = 0.8;
// =====
// specs
// -----
dfocal  = 4.5; // [cm]
l_endo  = 0.5;
l_epi   = 0.8;
mu_base = 110.0;

// mesh size [cm]
psize = 0.50;

Merge "prolate_mesh_nocap.geo";
