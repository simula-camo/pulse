Rin   = 1.5;
Rout  = 2.8;
psize = 0.25;

Point(1) = { Rin,  0.0, 0.0, psize };
Point(2) = { Rout, 0.0, 0.0, psize };

Line(1) = { 1, 2 };

line_id = 1;
For num In {0:2}
    out[] = Extrude
        { { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 }, Pi/2 }
        { Line{line_id}; };
    line_id = out[0];
    surf[num]    = out[1];
    bc_epi[num]  = out[2];
    bc_endo[num] = out[3];
EndFor
out[] = Extrude
    { { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 }, Pi/2 }
    { Line{line_id}; };
surf[num]    = out[1];
bc_epi[num]  = out[2];
bc_endo[num] = out[3];

Physical Surface(1) = { surf[] };
Physical Line(10)   = { bc_epi[] };
Physical Line(20)   = { bc_endo[] };

Mesh.ColorCarousel = 1;
Mesh.ElementOrder = 1;
Mesh.Lloyd = 1;
//Mesh.HighOrderOptimize = 2;
Mesh.Optimize = 1;

