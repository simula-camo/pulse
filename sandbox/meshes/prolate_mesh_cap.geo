// ========================
// generates a prolate mesh
// ------------------------

// x = d * sinh(lambda) * sin(mu) * cos(theta)
// y = d * sinh(lambda) * sin(mu) * sin(theta)
// z = d * cosh(lambda) * cos(mu)

// Note:
// - mesh is translated in order to have the base at z = 0
// - mu_epi such that z_endo = z_epi at the base

// Things
mu_endo_base = mu_base / 180.0 * Pi;
mu_apex = 0.0;

a_endo = dfocal * Sinh(l_endo);
b_endo = dfocal * Cosh(l_endo);

x_endo_base = a_endo * Sin(mu_endo_base);
y_endo_base = 0.0;
z_endo_base = b_endo * Cos(mu_endo_base);
x_endo_apex = a_endo * Sin(mu_apex);
y_endo_apex = 0.0;
z_endo_apex = b_endo * Cos(mu_apex);

a_epi = dfocal * Sinh(l_epi);
b_epi = dfocal * Cosh(l_epi);

mu_epi_base  = Acos(b_endo/b_epi * Cos(mu_endo_base));

x_epi_base = a_epi * Sin(mu_epi_base);
y_epi_base = 0.0;
z_epi_base = b_epi * Cos(mu_epi_base);
x_epi_apex = a_epi * Sin(mu_apex);
y_epi_apex = 0.0;
z_epi_apex = b_epi * Cos(mu_apex);

// Center
Point(1) = { 0.0, 0.0, - z_endo_base };

// Endocardium
Point(2) = { x_endo_base, y_endo_base, 0.0, psize };
Point(3) = { x_endo_apex, y_endo_apex, z_endo_apex - z_endo_base, psize };
Ellipse(1) = { 3, 1, 3, 2 };

// Epicardium
Point(4) = { x_epi_base, y_epi_base, 0.0, psize };
Point(5) = { x_epi_apex, y_epi_apex, z_epi_apex - z_epi_base, psize };
Ellipse(2) = { 5, 1, 5, 4 };

// Base
Line(3) = { 2, 4 };

// Cap
Point(6) = { 0.0, 0.0, 0.0, psize };
Line(4) = { 2, 6 };

// Extrusion
lid_endo = 1; lid_epi = 2; lid_base = 3; lid_cap = 4;
For num In {0:3}
  out[] = Extrude
    { { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 }, Pi/2}
    { Line{lid_endo}; };
  lid_endo = out[0];
  sid_endo[num] = out[1];
  out[] = Extrude
    { { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 }, Pi/2}
    { Line{lid_epi}; };
  lid_epi  = out[0];
  sid_epi[num] = out[1];
  out[] = Extrude
  { { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 }, Pi/2}
  { Line{lid_base}; };
  lid_base = out[0];
  sid_base[num] = out[1];
  out[] = Extrude
  { { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 }, Pi/2}
  { Line{lid_cap}; };
  lid_cap = out[0];
  sid_cap[num] = out[1];
EndFor

Compound Surface(100) = { sid_endo[] };
Compound Surface(200) = { sid_epi[] };
Compound Surface(300) = { sid_base[] };
Compound Surface(400) = { sid_cap[] };

// Full ventricle
Surface Loop(500) = { 100, 200, 300 };
Surface Loop(600) = { 100, 400 };

Volume(1000) = { 500 };
Volume(2000) = { 600 };

// Physical entities
// -----------------
Physical Volume(1) = { 1000 };
Physical Volume(2) = { 2000 };
// Base
Physical Surface(10) = { 300 };
// Cap
Physical Surface(40) = { 400 };
// Epi
Physical Surface(20) = { 200 };
// Endo
Physical Surface(30) = { 100 };
