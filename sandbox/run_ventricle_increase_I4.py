"""
Endocardium and epicardium rotates in opposite
direction. Is this because of I8?

Trying to increase I8 stiffness.
"""

import sys
from dolfin import *
from ventricle import *

# parameters
geometry_type = "prolate"
material_type = "holzapfelogden"
output_dir = "results/prolate_increase_I4"
postprocess = True

def main():

    if geometry_type == "disk" :
        from geometry import Disk
        geo = Disk()
    elif geometry_type == "prolate" :
        from geometry import Prolate
        geo = Prolate()
    else :
        return "geometry not implemented!"

    # material to use
    if material_type == "holzapfelogden" :
        from material import HolzapfelOgden
        mat = HolzapfelOgden(geo)
    else :
        return "material not implemented!"

    mat.params["a_sheets"] *= 5.0

    ventricle = Ventricle(geo, mat, output_dir, postprocess)
    ventricle.active_only_fibers = False

    if postprocess :
        from postprocess import ventricle_postprocess
        geo.construct_local_base()
        ventricle_postprocess(ventricle, output_dir)
    else :
        ventricle.cardiac_cycle()

    list_timings()

if __name__ == "__main__" :
    sys.exit(main())
