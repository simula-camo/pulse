.. pulse documentation master file, created by
   sphinx-quickstart on Thu Jan  8 14:14:21 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pulse's documentation!
=================================

Pulse is a small package for dealing with heart mechanics models
using `FEniCS`_. 

To download the source code or to report issues visit the `Bitbucket page
<https://bitbucket.org/peppu/pulse>`_

Contents:
=========

.. toctree::
   :maxdepth: 2
   :numbered:

   features
   demos
   reference
   contributing
   team

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _FEniCS: http://fenicsproject.org

Licence
=======
 
pulse is an open source project that can be freely used under the `GNU GPL version 3 licence`_.
 
.. _GNU GPL version 3 licence: http://www.gnu.org/licenses/gpl.html
