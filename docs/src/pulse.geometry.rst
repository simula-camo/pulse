Pulse geometries
================

LVGeometry
----------

.. automodule:: pulse.geometry.lvgeometry
    :members:
    :undoc-members:
    :show-inheritance:

LVEllipsoid
-----------

.. automodule:: pulse.geometry.lvellipsoid
    :members:
    :undoc-members:
    :show-inheritance:

LVProlateEllipsoid module
-------------------------

.. automodule:: pulse.geometry.lvprolateellipsoid
    :members:
    :undoc-members:
    :show-inheritance:

LVSimpleEllipsoid
-----------------

.. automodule:: pulse.geometry.lvsimpleellipsoid
    :members:
    :undoc-members:
    :show-inheritance:

BiVGeometry
-----------

.. automodule:: pulse.geometry.bivgeometry
    :members:
    :undoc-members:
    :show-inheritance:

BiVEllipsoid
------------

.. automodule:: pulse.geometry.bivellipsoid
    :members:
    :undoc-members:
    :show-inheritance:
