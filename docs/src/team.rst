Developers
==========

fenicshotools is developed by

* `Simone Pezzuto <http://icsweb.inf.unisi.ch/cms/index.php/people/150-simone-pezzuto.html>`_, Institute of Computational Science
* `Johan Hake <https://www.simula.no/people/hake>`_, Simula Research Laboratory
* `Henrik Nicolay Finsberg <henriknf@simula.no>`_, Simula Research Laboratory
* `Joakim Sundnes <sundnes@simula.no>`_, Simula Research Laboratory
